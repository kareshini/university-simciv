package model.mapsanalysis;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Dominik on 2016-11-11.
 */

public class NetBuilder {
    private int width;
    private int height;
    private int x;
    private int y;

    public NetBuilder width(int width){
        this.width=width;
        return this;
    }

    public NetBuilder height(int height){
        this.height=height;
        return this;
    }

    public NetBuilder x(int x){
        this.x = x;
        return this;
    }
    public NetBuilder y(int y){
        this.y =y;
        return this;
    }


    public BufferedImage injectToImg(File file) throws IOException {
        BufferedImage img = ImageIO.read(file);
        width = img.getWidth();
        height = img.getHeight();
        System.out.println(width);
        System.out.println(height);
        return drawLines(img);
    }

    private BufferedImage drawLines( BufferedImage image) {
        return drawLines(Color.BLACK,image);
    }
    private BufferedImage drawLines( Color lineColor , BufferedImage image){
        Graphics g = image.getGraphics();
        double offsetX = ((double)width)/((double)x);
        double offsetY = ((double)height)/((double)y);

        g.setColor(lineColor);

        for(int i = 1 ; i < x ; i++){
            g.drawLine((int) (i*offsetX),0,(int) (i*offsetX),height);
            System.out.println(Math.round(i*offsetX));
        }
        for(int i =1 ; i< y ; i++){
            g.drawLine(0,(int) (i*offsetY),width,(int) (i*offsetY));
        }

        return image;
    }
    public BufferedImage createWhiteImg(){
        BufferedImage img = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
        Graphics g = img.getGraphics();
        g.fillRect(0,0,width,height);
        return img;
    }
    public static void saveJPG(BufferedImage image,File savePathJPG) throws IOException {
        savePathJPG.createNewFile();
        ImageIO.write(image, "jpg", savePathJPG);
    }

    public static void main(String[] args) {
        NetBuilder b = new NetBuilder();

        BufferedImage img = b.width(1000).height(1000).x(100).y(100).createWhiteImg();
        b.drawLines(img);
        File f = new File("src/test/resources/model.mapsanalysis/net-created.jpg");
        try {
            b.saveJPG(img,f);
        } catch (IOException e) {
            e.printStackTrace();
        }

        File f2 = new File("src/test/resources/model.mapsanalysis/net-loaded.jpg");
        File tmpImg = new File("src/test/resources/model.mapsanalysis/testJPG.jpg");

        try {
            BufferedImage img2 = b.injectToImg(tmpImg); //it changes width and height
            b.drawLines(img2);
            b.saveJPG(img2,f2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
