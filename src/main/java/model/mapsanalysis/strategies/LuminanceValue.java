package model.mapsanalysis.strategies;

import java.awt.*;
import java.awt.image.BufferedImage;


/**
 * Created by Dominik on 2016-11-22.
 */
public class LuminanceValue implements PixelValueStrategy {
    float redIntensity = 0.2126f;
    float greenIntensity = 0.7152f;
    float blueIntensity = 0.0722f;
    public LuminanceValue() {
    }
    public LuminanceValue(float redIntensity, float greenIntensity , float blueIntensity ) {
        this.redIntensity=redIntensity;
        this.greenIntensity=greenIntensity;
        this.blueIntensity=blueIntensity;
    }

    @Override
    public float getPixelValue(BufferedImage image, int x, int y) {
        Color color = new Color(image.getRGB(x, y));

        int red   = color.getRed();
        int green = color.getGreen();
        int blue  = color.getBlue();

        //value of bright > value of dark
        return ((red * redIntensity + green * greenIntensity + blue * blueIntensity) / 255);
    }
}
