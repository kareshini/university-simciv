package model.mapsanalysis.strategies;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Dominik on 2017-01-06.
 */
public class HeatValue implements PixelValueStrategy {
    private final float MAX_HUE;
    private final float BLUE_HUE;
    private final float RED_HUE;

    public HeatValue() {
        float[] hsb = new float[3];
        Color.RGBtoHSB(255, 0, 255, hsb);
        MAX_HUE = hsb[0];

        Color.RGBtoHSB(0, 0, 255, hsb);
        BLUE_HUE = hsb[0];

        Color.RGBtoHSB(255, 0, 0, hsb);
        RED_HUE = hsb[0];
    }

    @Override
    public float getPixelValue(BufferedImage image, int x, int y) {
        Color rgb = new Color(image.getRGB(x, y));

        float[] hsb = new float[3];

        Color.RGBtoHSB(rgb.getRed(), rgb.getGreen(), rgb.getBlue(), hsb);
        float hue = hsb[0];
        float saturation = hsb[1];
        float brightness = hsb[2];


        //warunki przekroczenia granicy w zaleznosci od skali HSB
        if (hue > BLUE_HUE) {
            if (hue < MAX_HUE) {
                hue = BLUE_HUE;
            } else {
                hue = RED_HUE;
            }
        }
        if(saturation<0.1){
            return 0f;
        }

        //im mniej saturation tym jasniejszy kolor tym gorzej. Saturation <0,1>
        //im mniej brightness tym ciemniejszy kolor tym lepiej. Brightness <0,1>
        //ale trzeba sprawic ze jak brightness = 1 to jest optymalnie
        return 1.0f - (hue) * saturation * (2.0f - brightness) / 2;
    }}
