package model.mapsanalysis.strategies;

import java.awt.image.BufferedImage;

/**
 * Created by Dominik on 2016-11-22.
 */
public interface PixelValueStrategy {
    float getPixelValue(BufferedImage image, int x, int y);
}
