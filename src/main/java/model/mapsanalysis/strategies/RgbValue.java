package model.mapsanalysis.strategies;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Map;

/**
 * Created by Dominik on 2016-11-22.
 */
public class RgbValue implements PixelValueStrategy {

    private Map<Integer,Float> colorToValue;

    public RgbValue(Map<Integer,Float> colorToValue) {
        this.colorToValue=colorToValue;
    }

    @Override
    public float getPixelValue(BufferedImage image, int x, int y) {
        Integer color = image.getRGB(x, y);
        Color c = new Color(color);
//        System.out.println(x + " : " + y + " = (int) " +color + " , (col) r: " + c.getRed() +  " g:" + c.getGreen() + " b:" + c.getBlue() + " = (int)" + c.getRGB());
        Float returned = colorToValue.get(color);

//        String val =  prop.getProperty(factor.toString().toLowerCase() + "_" + String.valueOf(color.getRGB()));
        if(returned==null)
            return -1;
        else
            return returned;
    }

    public Map<Integer, Float> getColorToValueMap() {
        return colorToValue;
    }
}
