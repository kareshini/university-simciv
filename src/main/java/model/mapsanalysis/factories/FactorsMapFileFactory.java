package model.mapsanalysis.factories;

import model.util.Factors;

import java.io.File;

/**
 * Created by Dominik on 2016-11-20.
 */
public class FactorsMapFileFactory {
    public static File getFile(Factors factory){
        String base = "src/main/resources/factors/";
        String path = null;
        switch (factory){
            case WATER:
                path = base + "water.jpg";
                break;
            case WOOD:
                path = base + "woodlands.jpeg";
                break;
            case CLIMATE:
                path = base + "climat.jpg";
                break;
            case SOIL:
                path = base + "soilPH.jpg";
                break;
            case TEMPERATURE:
                path = base + "temperature.jpg";
                break;
            case HEIGHT:
                path = base + "height.jpg";
                break;
        }
        return new File(path);
    }
    public static Factors getFactory(File path){
        Factors factor = null;
        switch (path.getName()){
            case "water.jpg":
                factor = Factors.WATER;
                break;
            case "woodlands.jpeg":
                factor= Factors.WOOD;
                break;
            case "climat.jpg":
                factor= Factors.CLIMATE;
                break;
            case "soilPH.jpg":
                factor = Factors.SOIL;
                break;
            case "temperature.jpg":
                factor= Factors.TEMPERATURE;
                break;
            case "height.jpg":
                factor= Factors.HEIGHT;
                break;
        }
        return factor;
    }
}
