package model.mapsanalysis.factories;

import model.mapsanalysis.strategies.HeatValue;
import model.mapsanalysis.strategies.LuminanceValue;
import model.mapsanalysis.strategies.PixelValueStrategy;
import model.mapsanalysis.strategies.RgbValue;
import model.util.Factors;

import java.util.HashMap;
import java.util.Properties;

/**
 * Created by Dominik on 2016-11-25.
 */
public class StrategiesFactory {
    public static PixelValueStrategy getPixelValueStrategy(Factors mapFactory, Properties mapAnalysisProperties) {
        PixelValueStrategy strategy;
        String method = mapAnalysisProperties.getProperty(mapFactory.name().toLowerCase() + ".method");
        if(method==null){
            method="";
        }
        switch (method) {
            case "rgb":
                HashMap<Integer, Float> map = new HashMap<>();
                mapAnalysisProperties.keySet()
                        .stream()
                        .map(i -> ((String) i))
                        .filter(key -> key.startsWith(mapFactory.name().toLowerCase() + ".rgb."))
                        .forEach(key -> map.put(Integer.parseInt((key.substring(key.lastIndexOf(".") + 1))), Float.parseFloat(mapAnalysisProperties.getProperty(key))));

                strategy = new RgbValue(map);
                break;
            case "custom_luminance":
                String red = mapAnalysisProperties.getProperty(mapFactory.name().toLowerCase() + ".luminance.red");
                String green = mapAnalysisProperties.getProperty(mapFactory.name().toLowerCase() + ".luminance.green");
                String blue = mapAnalysisProperties.getProperty(mapFactory.name().toLowerCase() + ".luminance.blue");

                strategy = new LuminanceValue(Float.parseFloat(red),Float.parseFloat(green),Float.parseFloat(blue));
                break;
            case "heat":
                strategy = new HeatValue();
                break;
            default:
                strategy = new LuminanceValue();
                break;
        }
        return strategy;
    }
}
