package model.mapsanalysis;

import model.cell.CellCoordinates;
import model.mapsanalysis.factories.FactorsMapFileFactory;
import model.mapsanalysis.factories.StrategiesFactory;
import model.mapsanalysis.strategies.PixelValueStrategy;
import model.util.Factors;
import model.util.ImgUtil;
import model.util.PropertiesFiles;
import model.util.PropertiesUtil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Dominik on 2016-11-20.
 */
public class FactorReader {
    private BufferedImage image;
    private Color irrelevant;
    private double offsetX, offsetY;
    private PixelValueStrategy strategy;

    public static FactorReader createInstance(Factors mapFactory, int amountX, int amountY) throws IOException {
        BufferedImage image = ImageIO.read(FactorsMapFileFactory.getFile(mapFactory));
        Properties mapAnalysisProperties = PropertiesUtil.getProperties(PropertiesFiles.MAP_ANALYSIS);

        String irrelevantColor = mapAnalysisProperties.getProperty(mapFactory.name().toLowerCase() + ".irrelevant");
        Color irrelevant;
        if (irrelevantColor != null) {
            irrelevant = new Color(Integer.parseInt(irrelevantColor));
        } else {
            irrelevant = ImgUtil.getTheBrightestColor(image);
        }

        PixelValueStrategy strategy = StrategiesFactory.getPixelValueStrategy(mapFactory, mapAnalysisProperties);

        double offsetX = image.getWidth() / amountX;
        double offsetY = image.getHeight() / amountY; // int/double = double

        return new FactorReader(image, irrelevant, strategy, offsetX, offsetY);
    }

    private FactorReader(BufferedImage image, Color irrelevant, PixelValueStrategy strategy, double offsetX, double offsetY) {
        this.image = image;
        this.irrelevant = irrelevant;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.strategy = strategy;
    }

    private double getAvgValue(CellCoordinates coordinates) {
        double startX, startY;
        startX = (coordinates.x * offsetX );
        startY = (coordinates.y * offsetY );

        double sum = 0;

        int maxI = (int) (startX + offsetX);
        int maxJ = (int) (startY + offsetY);
        int counter = 0;
        float pixelVal = 0;

        for (int i = (int) startX; i < maxI; i++) {
            for (int j = (int) startY; j < maxJ; j++) {
                if (image.getRGB(i, j) != irrelevant.getRGB()) {
                    pixelVal = strategy.getPixelValue(image, i, j);
                    if (pixelVal > 0) {
                        sum += pixelVal;
                        counter++;
                    }
                }
            }
        }
        return sum / counter;
    }

    public boolean isContinent(CellCoordinates coordinates) {
        double startX, startY;
        startX = (coordinates.x * offsetX );
        startY = (coordinates.y * offsetY );

        int maxI = (int) (startX + offsetX);
        int maxJ = (int) (startY + offsetY);
        int counter = 0, c = 0;

        for (int i = (int) startX; i < maxI; i++) {
            for (int j = (int) startY; j < maxJ; j++) {
                if (image.getRGB(i, j) == irrelevant.getRGB())
                    counter++;
                c++;
            }
        }

        if (counter > c / 2)
            return false;
        else
            return true;
    }

    public int getFactoryValue(CellCoordinates cellCoordinates) throws OutOfContinentException {
        if (isContinent(cellCoordinates))
            return (int) (getAvgValue(cellCoordinates) * 100);
        else
            throw new OutOfContinentException();
    }

    public static void main(String[] args) throws IOException {
        System.out.println(ImgUtil.rgbToInt(191,202,204));
    }

    private static BufferedImage readMap(File factoryMap) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(factoryMap);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }

    public static void readFactory(Factors mapFactory, int x, int y) throws IOException {

        FactorReader reader = FactorReader.createInstance(mapFactory, x, y);
        System.out.println(reader);
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                try {
                    System.out.println("i=" + i + " j=" + j + " val=" + reader.getFactoryValue(new CellCoordinates(i, j)));
                } catch (OutOfContinentException e) {
                    System.out.println("i=" + i + " j=" + j + " val=is not inside the cotinent");
                }
            }
        }
    }

}
