package model.agent;

import com.google.common.collect.BiMap;
import model.agent.actions.*;
import model.cell.Cell;
import model.cell.CellCoordinates;
import model.civilisation.Marker;
import model.agent.traits.AgentTraits;
import model.util.Configuration;
import model.util.Constants;

import java.util.Set;

/**
 * Created by majk on 19.11.16.
 */

public class Agent {

    public final int maxPop = Integer.parseInt(Configuration.property(Constants.CELL, "population.max"));

    public AgentTraits traits;
    private int population;
    private double saturation;
    private double happiness;
    private Marker civilisation;
    private AgentActions action;
    private ActionStrategy strategy;
    private final int id;

    public Agent(Marker marker, int id) {
        traits = new AgentTraits();
        saturation = 0.0;
        population = 0;
        civilisation = marker;
        action = AgentActions.NONE;
        happiness = 0.0;
        this.id = id * 29 + 13;
    }

    public void updatePopulation(int byValue) {
        population += byValue;
        if(population > maxPop) population = maxPop;
        if(population < 0) population = 0;
        updateSaturation();
    }

    public int getPopulation() {
        return population;
    }

    public double getSaturation() {
        return saturation;
    }

    public AgentActions getAction() {return action;}

    private void updateSaturation() {
        if(population < (maxPop / 10) && population > 0)
            saturation = 0.1;
        else
            saturation = (double) population / (double) maxPop;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Agent agent = (Agent) o;

        if (maxPop != agent.maxPop) return false;
        if (population != agent.population) return false;
        if (Double.compare(agent.saturation, saturation) != 0) return false;
        if (traits != null ? !traits.equals(agent.traits) : agent.traits != null) return false;
        return civilisation == agent.civilisation;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public Marker getCivilisation() {
        return civilisation;
    }

    public void setCivilisation(Marker civilisation) {
        this.civilisation = civilisation;
    }

    public void performAction(BiMap<CellCoordinates, Cell> map,
                              BiMap<CellCoordinates, Agent> agentMap) {
        strategy.perform(map, agentMap);
        happiness = traits.socialSatisfaction(population, action);
        //updatePopulation(happiness * sth * population);
    }

    public void setAction(BiMap<CellCoordinates, Cell> map,
                          BiMap<CellCoordinates, Agent> agentMap) {

        CellCoordinates location = agentMap.inverse().get(this);
        Cell currentCell = map.get(location);
        Set<CellCoordinates> neighbors = MigrationHelper.getNeighbors(map, currentCell);
        action = calculateBestAction(agentMap, neighbors);
    }

    private AgentActions calculateBestAction(BiMap<CellCoordinates, Agent> agentMap,
                                             Set<CellCoordinates> neighbors) {

        if(population > 0.9 * maxPop) {
            strategy = new MigrationPerformer(this);
            return AgentActions.MIGRATION;
        }

        double military = traits.militaryPower();
        double economic = traits.economicPower();

        if(military > economic)
        {
            int enemies = countAgents(neighbors, agentMap, true);
            if(enemies > neighbors.size() / 2) {
                strategy = new AttackPerformer(this);
                return AgentActions.ATTACK;
            }
            else {
                strategy = new MilitaryDevelop(this);
                return AgentActions.MILITARY_DEVELOPMENT;
            }
        }
        else
        {
            int allies = countAgents(neighbors, agentMap, false);
            if(allies > neighbors.size() / 2) {
                strategy = new TradePerformer(this);
                return AgentActions.TRADE;
            }
            else {
                strategy = new EconomicDevelop(this);
                return AgentActions.ECONOMIC_DEVELOPMENT;
            }
        }
    }

    //todo reimplement with allies marker
    private int countAgents(Set<CellCoordinates> neighbors,
                            BiMap<CellCoordinates, Agent> agentMap,
                            boolean enemies) {

        int result = 0;
        for(CellCoordinates neighbor : neighbors) {
            Agent a = agentMap.get(neighbor);
            if(enemies && a.getCivilisation() != civilisation)
                result++;
            else if (!enemies && a.getCivilisation() == civilisation)
                result++;
        }

        return result;
    }
}
