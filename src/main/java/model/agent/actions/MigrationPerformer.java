package model.agent.actions;

import com.google.common.collect.BiMap;
import model.agent.Agent;
import model.agent.MigrationHelper;
import model.agent.traits.EconomicTraits;
import model.agent.traits.MilitaryTraits;
import model.cell.Cell;
import model.cell.CellCoordinates;
import model.civilisation.Marker;

/**
 * Created by majk on 06.01.17.
 */
public class MigrationPerformer extends ActionStrategy {

    public MigrationPerformer(Agent a) {
        super(a);
    }


    @Override
    public void perform(BiMap<CellCoordinates, Cell> map,
                        BiMap<CellCoordinates, Agent> agentMap) {

        CellCoordinates location = agentMap.inverse().get(agent);
        Cell currentCell = map.get(location);
        CellCoordinates bestLocation = MigrationHelper.getBestCellLocation(map, agentMap, currentCell);

        if (bestLocation != null) {
            Agent targetAgent = agentMap.get(bestLocation);

            if (targetAgent.getCivilisation() == Marker.NONE) {
                migrate(targetAgent, 2, false);
            } else {
                double scale = 1 - (agent.getPopulation() / targetAgent.getPopulation());
                migrate(targetAgent, scale, true);
            }
        }
    }

    private void migrate(Agent targetAgent, double scale, boolean settled) {

        if(!settled) targetAgent.setCivilisation(agent.getCivilisation());
        int moving = (int)(agent.getPopulation() / scale);

        double soldiers = agent.traits.military.getValue(MilitaryTraits.SOLDIERS) / scale;
        double tech = agent.traits.military.getValue(MilitaryTraits.TECHNOLOGY) / scale;
        double weapons = agent.traits.military.getValue(MilitaryTraits.WEAPONRY) / scale;

        double export = agent.traits.economic.getValue(EconomicTraits.EXPORT) / scale;
        double imp = agent.traits.economic.getValue(EconomicTraits.IMPORT) / scale;
        double serv = agent.traits.economic.getValue(EconomicTraits.SERVICES) / scale;

        updateCurrent(soldiers, tech, weapons, export, imp, serv);

        agent.updatePopulation(-moving);
        targetAgent.updatePopulation(moving);

        updateTarget(targetAgent, soldiers, tech, weapons, export, imp, serv);
    }

    private void updateCurrent(double soldiers, double tech, double weapons,
                               double export, double imp, double serv) {

        updateTarget(agent, soldiers, tech, weapons, export, imp, serv);
    }

    private void updateTarget(Agent targetAgent,
                              double soldiers, double tech, double weapons,
                              double export, double imp, double serv) {

        targetAgent.traits.military.changeValue(MilitaryTraits.SOLDIERS, soldiers);
        targetAgent.traits.military.changeValue(MilitaryTraits.WEAPONRY, weapons);
        targetAgent.traits.military.changeValue(MilitaryTraits.TECHNOLOGY, tech);

        targetAgent.traits.economic.changeValue(EconomicTraits.EXPORT, export);
        targetAgent.traits.economic.changeValue(EconomicTraits.EXPORT, imp);
        targetAgent.traits.economic.changeValue(EconomicTraits.SERVICES, serv);
    }

}
