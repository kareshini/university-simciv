package model.agent.actions;

import model.agent.Agent;

/**
 * Created by majk on 06.01.17.
 */
public abstract class ActionStrategy implements IAgentAction {
    protected Agent agent;

    public ActionStrategy(Agent a) {
        agent = a;
    }
}
