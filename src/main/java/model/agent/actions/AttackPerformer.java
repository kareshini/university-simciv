package model.agent.actions;

import com.google.common.collect.BiMap;
import model.agent.Agent;
import model.agent.MigrationHelper;
import model.agent.traits.MilitaryTraits;
import model.agent.traits.TraitHelper;
import model.cell.Cell;
import model.cell.CellCoordinates;

import java.util.Set;

/**
 * Created by majk on 06.01.17.
 */
public class AttackPerformer extends ActionStrategy {

    public AttackPerformer(Agent a) {
        super(a);
    }

    @Override
    public void perform(BiMap<CellCoordinates, Cell> map,
                        BiMap<CellCoordinates, Agent> agentMap) {

        Agent enemy = getWeakestEnemy(map, agentMap);
        double myArmy = agent.traits.military.getValue(MilitaryTraits.SOLDIERS);
        double enemyArmy = enemy.traits.military.getValue(MilitaryTraits.SOLDIERS);

        double myWeaponry = agent.traits.military.getValue(MilitaryTraits.WEAPONRY);
        double enemyWeaponry = enemy.traits.military.getValue(MilitaryTraits.WEAPONRY);

        double myPeople = agent.getPopulation();
        double enemyPeople = enemy.getPopulation();

        int myLoss = (int)(enemyArmy * 0.5 * enemyPeople);
        int enemyLoss = (int)(myArmy * myPeople);

        agent.updatePopulation(-myLoss);
        enemy.updatePopulation(-enemyLoss);

        double tmpWep, tmpArmy;
        tmpArmy = myArmy;
        double enemyPower = enemy.traits.militaryPower();
        myArmy = myArmy - (enemyArmy * enemyPower);

        tmpWep = myWeaponry;
        myWeaponry -= enemyWeaponry;

        enemyWeaponry -= tmpWep;
        enemyArmy = enemyArmy - tmpArmy;

        agent.traits.military.changeValue(MilitaryTraits.SOLDIERS, myArmy);
        enemy.traits.military.changeValue(MilitaryTraits.SOLDIERS, enemyArmy);

        agent.traits.military.changeValue(MilitaryTraits.WEAPONRY, myWeaponry);
        enemy.traits.military.changeValue(MilitaryTraits.WEAPONRY, enemyWeaponry);

    }

    private Agent getWeakestEnemy(BiMap<CellCoordinates, Cell> map,
                                             BiMap<CellCoordinates, Agent> agentMap) {

        double minimum = Double.MAX_VALUE;
        Agent result = null;

        CellCoordinates location = agentMap.inverse().get(agent);
        Cell currentCell = map.get(location);
        Set<CellCoordinates> neighbors = MigrationHelper.getNeighbors(map, currentCell);

        for (CellCoordinates neighbor : neighbors) {
            Agent a = agentMap.get(neighbor);
            if(a.getCivilisation() == agent.getCivilisation()) continue;

            double power = a.traits.militaryPower();
            if(power < minimum) {
                minimum = power;
                result = a;
            }
        }
        return result;
    }
}
