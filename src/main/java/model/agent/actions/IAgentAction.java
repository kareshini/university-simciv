package model.agent.actions;

import com.google.common.collect.BiMap;
import model.agent.Agent;
import model.cell.Cell;
import model.cell.CellCoordinates;

/**
 * Created by majk on 06.01.17.
 */
public interface IAgentAction{
    void perform(BiMap<CellCoordinates, Cell> map,
                 BiMap<CellCoordinates, Agent> agentMap);
}
