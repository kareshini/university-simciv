package model.agent.actions;

import com.google.common.collect.BiMap;
import model.agent.Agent;
import model.agent.traits.EconomicTraits;
import model.agent.traits.TraitHelper;
import model.cell.Cell;
import model.cell.CellCoordinates;

/**
 * Created by majk on 06.01.17.
 */
public class EconomicDevelop extends ActionStrategy {

    public EconomicDevelop(Agent a) {
        super(a);
    }

    @Override
    public void perform(BiMap<CellCoordinates, Cell> map,
                        BiMap<CellCoordinates, Agent> agentMap) {

        int population = agent.getPopulation();
        double development = population / agent.maxPop;

        if(population > agent.maxPop / 2) {
            agent.traits.economic.updateValue(EconomicTraits.EXPORT, development * 2);
            agent.traits.economic.updateValue(EconomicTraits.IMPORT, -development);
        } else {
            agent.traits.economic.updateValue(EconomicTraits.IMPORT, development * 2);
            agent.traits.economic.updateValue(EconomicTraits.EXPORT, -development);
        }

        double exp = agent.traits.economic.getValue(EconomicTraits.EXPORT);
        double imp = agent.traits.economic.getValue(EconomicTraits.IMPORT);

        double serv = (exp - imp) * 10;

        agent.traits.economic.changeValue(EconomicTraits.SERVICES, serv);
    }
}
