package model.agent.actions;

import com.google.common.collect.BiMap;
import model.agent.Agent;
import model.agent.traits.MilitaryTraits;
import model.agent.traits.TraitHelper;
import model.cell.Cell;
import model.cell.CellCoordinates;

/**
 * Created by majk on 06.01.17.
 */
public class MilitaryDevelop extends ActionStrategy {

    public MilitaryDevelop(Agent a) {
        super(a);
    }

    @Override
    public void perform(BiMap<CellCoordinates, Cell> map,
                        BiMap<CellCoordinates, Agent> agentMap) {

        //todo affect cell

        int population = agent.getPopulation();
        double development = population / agent.maxPop;

        agent.traits.military.updateValue(MilitaryTraits.WEAPONRY, development);
        agent.traits.military.updateValue(MilitaryTraits.TECHNOLOGY, development * 2);

        double weapons = agent.traits.military.getValue(MilitaryTraits.WEAPONRY);
        double tech = agent.traits.military.getValue(MilitaryTraits.TECHNOLOGY);

        double mean = (weapons + tech) / 2;
        double soldiers = population * mean / agent.maxPop;

        agent.traits.military.changeValue(MilitaryTraits.SOLDIERS, soldiers);
    }
}
