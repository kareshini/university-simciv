package model.agent.traits;

/**
 * Created by majk on 19.11.16.
 */
public enum EconomicTraits implements Trait {
    SERVICES, EXPORT, IMPORT
}
