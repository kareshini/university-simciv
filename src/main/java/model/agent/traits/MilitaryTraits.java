package model.agent.traits;

/**
 * Created by majk on 19.11.16.
 */
public enum MilitaryTraits implements Trait {
    SOLDIERS, TECHNOLOGY, WEAPONRY
}
