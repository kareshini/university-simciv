package model.agent.traits;

import model.agent.AgentActions;
import model.util.Configuration;
import model.util.Constants;

/**
 * Created by majk on 19.11.16.
 */
public class AgentTraits {
    public TraitHelper<EconomicTraits> economic;
    public TraitHelper<MilitaryTraits> military;
    private int maxPop;

    public AgentTraits() {
        economic = new TraitHelper(EconomicTraits.class);
        military = new TraitHelper(MilitaryTraits.class);
        maxPop = Integer.parseInt(Configuration.property(Constants.CELL, "population.max"));
    }

    public double militaryPower() {
        double soldiers = military.getValue(MilitaryTraits.SOLDIERS);
        double tech = military.getValue(MilitaryTraits.TECHNOLOGY);
        double weapons = military.getValue(MilitaryTraits.WEAPONRY);

        double result = (tech+weapons) * soldiers == 0 ? (tech+weapons) * soldiers : tech+weapons;

        return normalize(result);
    }

    public double economicPower() {
        double export = economic.getValue(EconomicTraits.EXPORT);
        double imp = economic.getValue(EconomicTraits.IMPORT);
        double serv = economic.getValue(EconomicTraits.SERVICES);

        double result = (export+imp) * serv == 0 ? (export+imp) * serv : export+imp;

        return normalize(result);
    }

    public double socialSatisfaction(double population, AgentActions action) {
        double factor;
        switch(action){
            case ATTACK:
                factor = 0.1;
                break;
            case MIGRATION:
                factor = 0.4;
                break;
            case TRADE:
                factor = 1;
                break;
            case ECONOMIC_DEVELOPMENT:
                factor = 0.7;
                break;
            default :
                factor = 0.5;
                break;
        }
        return (population / maxPop) * factor;
    }

    private double normalize(double value) {
        return value / 200;
    }
}
