package model.agent.traits;

import model.util.Configuration;
import model.util.Constants;

import java.util.EnumMap;
import java.util.List;

/**
 * Created by majk on 19.11.16.
 */
public class TraitHelper <T extends Enum<T>> {
    private EnumMap <T, Double> factors;
    private final static double MaxVal = Double.parseDouble(Configuration.property(Constants.CELL, "traits.max"));

    public TraitHelper(Class<T> traits) {
        factors = new EnumMap<>(traits);
        for (T t : traits.getEnumConstants()) {
            factors.put(t, 0.0);
        }
    }

    public TraitHelper(Class<T> traits, List<Double> values) {
        try {
            FillMap(traits, values);
        }catch(TraitBondsException ex) {
            factors = null;
            ex.printStackTrace();
        }
    }

    /**
     * Replaces current value.
     * Result is non-negative and won't exceed defined
     * maximum read from properties.
     *
     * @param t Trait type
     * @param val New value
     */

    public void changeValue(T t, double val) {
        putValue(t, val);
    }

    /**
     * Increases/decreases current value by given amount.
     * Result is non-negative and won't exceed defined
     * maximum read from properties.
     *
     * @param t Trait type
     * @param val Change to the current value
     */
    public void updateValue(T t, double val) {
        val = factors.get(t) + val;
        putValue(t, val);
    }

    public double getValue(T t) {
        return factors.get(t);
    }

    private void FillMap(Class<T> traits, List<Double> values) throws TraitBondsException{
        factors = new EnumMap<>(traits);
        if (values.size() != traits.getEnumConstants().length)
            throw new TraitBondsException("Differing cardinality of values and enum constants");
        int it = 0;
        for (T t : traits.getEnumConstants()) {
            factors.put(t, values.get(it++));
        }
    }

    private void putValue(T t, double val) {
        if(val > MaxVal) val = MaxVal;
        if(val < 0) val = 0.0;
        factors.put(t, val);
    }

    public static class TraitBondsException extends Exception {
        public TraitBondsException(String message) {
            super(message);
        }
    }
}
