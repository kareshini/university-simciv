package model.agent;

/**
 * Created by majk on 06.01.17.
 */
public enum AgentActions {
    NONE, MIGRATION,
    MILITARY_DEVELOPMENT, ATTACK,
    TRADE, ECONOMIC_DEVELOPMENT
}
