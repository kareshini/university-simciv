package model.agent;

import com.google.common.collect.BiMap;
import model.cell.Cell;
import model.cell.CellCoordinates;
import model.cell.CellState;
import model.cell.Neighborhood;
import model.civilisation.Marker;
import model.util.Configuration;
import model.util.Constants;

import java.util.Set;

/**
 * Created by majk on 08.12.16.
 *
 * One-instance
 */

public final class MigrationHelper {
    private static MigrationHelper instance;
    private static Neighborhood strategy;
    private static int maxPop;

    private MigrationHelper() {}

    public static void initialize(Neighborhood n) {
        if (instance != null) {
            throw new MigratorInitException("Migrator has already been initialized");
        }
        instance = new MigrationHelper();
        strategy = n;
        maxPop = Integer.parseInt(Configuration.property(Constants.CELL, "population.max"));
    }

    public static Set<CellCoordinates> getNeighbors(BiMap<CellCoordinates, Cell> map, Cell cell) {
        CellCoordinates location = map.inverse().get(cell);
        return instance().strategy.getNeighbors(location);
    }

    public static CellCoordinates getBestCellLocation(BiMap<CellCoordinates, Cell> map, BiMap<CellCoordinates, Agent> agentMap,
                                               Cell cell) {

        CellCoordinates cellLocation = map.inverse().get(cell);
        Marker civ = agentMap.get(cellLocation).getCivilisation();
        Set<CellCoordinates> neighbors = getNeighbors(map, cell);
        CellCoordinates result = null;
        double bestValue = 0.0;

        for (CellCoordinates neighbor : neighbors) {
            Marker cellCiv = agentMap.get(neighbor).getCivilisation();
            if(cellCiv != civ && cellCiv != Marker.NONE) continue;

            Cell currentCell = map.get(neighbor);
            double currentValue = currentCell.getOverallCellValue();
            currentValue = adjustValueForDiagonals(currentValue, cellLocation, neighbor);

            Agent currentAgent = agentMap.get(neighbor);
            if(currentAgent != null) {
                int currentPopulation = currentAgent.getPopulation();
                currentValue = adjustValueForPopulation(currentValue, currentPopulation);
            }

            if (currentValue < bestValue || currentCell.getCellState().equals(CellState.WATER)) continue;
            bestValue = currentValue;
            result = neighbor;
        }
        return result;
    }

    private static MigrationHelper instance() {
        if (instance == null) {
            throw new MigratorInitException("Migrator not initialized. Use initialize() before access");
        }
        return instance;
    }

    private static double adjustValueForPopulation(double value, int population){
        double factor;
        if(population > maxPop/2)
            factor = (-2 / maxPop) * population + 2;
        else
            factor = (maxPop/0.9) * population + 0.1;

        return value * factor;
    }

    private static double adjustValueForDiagonals(double value, CellCoordinates cell, CellCoordinates neighbor) {
        if(isDiagonal(cell, neighbor)) {
            return value / Math.sqrt(2);
        }
        return value;
    }

    private static boolean isDiagonal(CellCoordinates cell, CellCoordinates neighbor) {
        int difference =
                Math.abs(cell.x - neighbor.x) +
                Math.abs(cell.y - neighbor.y);

        return difference == 2;
    }

    public static class MigratorInitException extends ExceptionInInitializerError {
        public MigratorInitException(String message) {
            super(message);
        }
    }
}
