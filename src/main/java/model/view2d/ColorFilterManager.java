package model.view2d;

import model.cell.CellCoordinates;
import model.cell.CellState;
import model.util.ImgUtil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Dominik on 2016-11-29.
 */
public class ColorFilterManager {
    private BufferedImage image;
    private double offsetX, offsetY;
    private Rectangle boundary;

    public ColorFilterManager(BufferedImage africaImage,int x ,int y) { //there is no sens to make it more universal. This class will be only used with africaMain.jpg
        this.image = africaImage;
        this.offsetX = (image.getWidth())/x;
        this.offsetY = (image.getHeight())/y;

    }

    public void setColorFilter(CellCoordinates cellCoordinate, int filterMask){
        int startX = (int) (offsetX*cellCoordinate.x);
        int startY = (int) (offsetY*cellCoordinate.y);
        int x;
        int y;

        for(int i = 0; i<offsetX;i++){
            for(int j=0; j<offsetY;j++){
                x = startX +i;
                y = startY +j;
                image.setRGB(x,y,image.getRGB(x,y)&filterMask);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedImage image = ImageIO.read(new File("src/main/resources/africaMap.jpg"));

        ColorFilterManager cfm = new ColorFilterManager(image,100,100);
        for(int i=0; i<50;i++){
            for(int j=0; j<50;j++){
                cfm.setColorFilter(new CellCoordinates(i,j),FilterMaskFactory.getMaskColorFitler(CellState.FOREST));
            }
        }
        for(int i=50; i<100;i++){
            for(int j=0; j<50;j++){
                cfm.setColorFilter(new CellCoordinates(i,j),FilterMaskFactory.getMaskColorFitler(CellState.DESERT));
            }
        }
        for(int i=0; i<50;i++){
            for(int j=50; j<100;j++){
                cfm.setColorFilter(new CellCoordinates(i,j),FilterMaskFactory.getMaskColorFitler(CellState.STEPPE));
            }
        }
        for(int i=50; i<100;i++){
            for(int j=50; j<100;j++){
                cfm.setColorFilter(new CellCoordinates(i,j),FilterMaskFactory.getMaskColorFitler(CellState.MOUNTAIN));
            }
        }
        ImgUtil.saveJPG(image,new File("src/test/resources/model.view2d/colorFilters.jpg"));
    }
}
