package model.view2d;

import model.cell.CellState;

/**
 * Created by Dominik on 2016-11-29.
 */
/*
int = 4 bajty = 32 bity
A R G B
 */
public class FilterMaskFactory {
    public static int getMaskColorFitler(CellState cellState){
        switch (cellState){
            case WATER:
                return 0x000000FF;
            case STEPPE:
                return 0x0000FFFF;
            case AGRICULTURAL:
                return 0x00FFFFFF;
            case FOREST:
                return 0x0000FF00;
            case DESERT:
                return 0x00FFFF00;
            case MOUNTAIN:
                return 0x00FF0000;
            default:
                return 0xFFFFFFFF;
        }
    }
}
