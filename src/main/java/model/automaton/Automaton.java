package model.automaton;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import model.agent.Agent;
import model.agent.MigrationHelper;
import model.cell.Cell;
import model.cell.CellCoordinates;
import model.cell.MoorNeighborhood;
import model.cell.Neighborhood;
import model.civilisation.Civilisation;

import java.util.*;

/**
 * Created by michal on 10.12.16.
 */
public class Automaton {
    private BiMap<CellCoordinates, Cell> cellMap;
    private BiMap<CellCoordinates, Agent> agentMap;
    private Set<Civilisation> civilisations;
    private Neighborhood neighborhoodStrategy;
    private int w, h;

    public Automaton(BiMap<CellCoordinates, Cell> cellMap, BiMap<CellCoordinates, Agent> agentMap,
                     Set<Civilisation> civilisations,
                     int w, int h) {
        this.cellMap = cellMap;
        this.agentMap = agentMap;
        this.civilisations = civilisations;
        this.w = w;
        this.h = h;
        this.neighborhoodStrategy = new MoorNeighborhood(w, h);
        MigrationHelper.initialize(neighborhoodStrategy);
    }

    private Automaton nextStep(){
        Set<Civilisation> updatedCivSet = new HashSet<>();
        BiMap<CellCoordinates, Cell> updatedMap = HashBiMap.create();
        BiMap<CellCoordinates, Agent> updatedAgentMap = HashBiMap.create();
        updatedMap.putAll(cellMap);
        update(updatedCivSet, updatedMap, updatedAgentMap);

        return new Automaton(updatedMap, updatedAgentMap ,updatedCivSet, w, h);

    }

    private void update(Set<Civilisation> updatedCivSet, BiMap<CellCoordinates, Cell> updatedMap,
                        BiMap<CellCoordinates, Agent> updatedAgentMap){

        BiMap<Agent, CellCoordinates> revAgentMap = agentMap.inverse();
        civilisations.forEach(c -> {
            Civilisation updatedCivilisation = new Civilisation(c.country);

            Iterator<Agent> agIt = c.getIterator();
            while ( agIt.hasNext()){
                Agent agent = agIt.next();
                CellCoordinates coords = revAgentMap.get(agent);

                Cell updatedCell = cellUpdate(agent, coords, neighborhoodStrategy.getNeighbors(coords));
                updatedMap.replace(coords, updatedCell);

                Agent updatedAgent = agentUpdate(agent, neighborhoodStrategy.getNeighbors(coords));
                updatedCivilisation.addAgent(updatedAgent);
                updatedAgentMap.put(coords, agent);
            }

            updatedCivSet.add(updatedCivilisation);
        });
    }

    private Cell cellUpdate(Agent agent, CellCoordinates coords, Set<CellCoordinates> neighbors){
        Cell updatedCell = cellMap.get(coords);
        updatedCell.performUpdates(agent, neighbors, cellMap);
        return updatedCell;
    }

    private Agent agentUpdate(Agent agent, Set<CellCoordinates> neighbors) {
        Agent updatedAgent = agent;
        //todo update logic, migrations etc
        return updatedAgent;
    }
}
