package model.civilisation;

/**
 * Created by majk on 19.11.16.
 */

/* should only be colors which aren't used in terrain elevation visualisation( e.g. yellow, green, orange etc.)
Please be forgiving for the names, I just googled these colors xD
 */
public enum Marker {
     NONE, PURPLE, BLACK, MAROON, OLIVE
}
