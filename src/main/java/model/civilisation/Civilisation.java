package model.civilisation;

import model.agent.Agent;
import model.cell.CellCoordinates;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by majk on 26.11.16.
 */
public class Civilisation {
    public Marker country;
    private List<Agent> agents;

    public Civilisation(Marker m) {
        country = m;
        agents = new ArrayList<>();
    }

    public Civilisation(Marker m, List<Agent> _agents) {
        country = m;
        agents = _agents;
    }

    public void addAgent(Agent agent){
        agents.add(agent);
    }

    /*public void updatePopulation(CellCoordinates cell, int change) {
        agents.forEach((agent) -> {
            if(agent.coordinates == cell) {
                agent.updatePopulation(change);
            }
        });
    }*/

    public Iterator<Agent> getIterator(){
        return agents.iterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Civilisation that = (Civilisation) o;

        if (country != that.country) return false;
        return agents != null ? agents.equals(that.agents) : that.agents == null;

    }

    @Override
    public int hashCode() {
        int result = country != null ? country.hashCode() : 0;
        result = 31 * result + (agents != null ? agents.hashCode() : 0);
        return result;
    }
}
