package model.cell;

/**
 * Created by michal on 19.11.16.
 */
public final class CellCoordinates {
    public int x;
    public int y;

    public CellCoordinates(int _x, int _y){
        x = _x;
        y = _y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CellCoordinates that = (CellCoordinates) o;

        return y == that.y && x == that.x;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    public CellCoordinates(CellCoordinates c) {
        this.x = c.x;
        this.y = c.y;
    }
}
