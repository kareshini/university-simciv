package model.cell;

import java.util.Set;

/**
 * Created by majk on 08.12.16.
 */
public abstract class Neighborhood {

    private final int width;
    private final int height;

    public Neighborhood(int w, int h) {
        width = w;
        height = h;
    }

    protected CellCoordinates getCoords(int x, int y) {
        if( x >= width || y >= height || x < 0 || y < 0 )
            return null;
        return new CellCoordinates(x, y);
    }

    public abstract Set<CellCoordinates> getNeighbors(CellCoordinates coords);
}
