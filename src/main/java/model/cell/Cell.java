package model.cell;
import com.google.common.collect.BiMap;
import model.agent.Agent;
import model.cell.modifiers.FertilityUpdate;
import model.cell.modifiers.CellModifier;
import model.util.Configuration;
import model.util.Factors;
import model.util.Constants;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by michal on 19.11.16.
 */
public class Cell {

    private CellState cellState;
    private List<CellModifier> cellModifiers;


    private Map<Factors, Integer> factors;

    private final int id;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cell cell = (Cell) o;

        return id == cell.id;

    }

    @Override
    public int hashCode() {
        return id;
    }


    //todo take into considaration new parametrs
    public Cell(CellState _cellState,int id, Map<Factors,Integer> factors){
        /*fertility = factors.get(Factors.SOIL);
        accessToWater = factors.get(Factors.WATER);
        elevation = factors.get(Factors.HEIGHT);
        temperature= factors.get(Factors.TEMPERATURE);
        climat= factors.get(Factors.CLIMATE);*/

        this.factors = factors;
        cellState = _cellState;
        this.id = id * 31 + 7;
        cellModifiers.add(new FertilityUpdate(this));

        validateParameters();
    }

    public void performUpdates(Agent agent, Set<CellCoordinates> neighbors, BiMap<CellCoordinates, Cell> cellMap){
        cellModifiers.forEach(modifier -> modifier.perform(agent, neighbors, cellMap));
    }



    public int getFactor(Factors factor){
        return  factors.get(factor);
    }
    public void  setFactors(Factors factor, int value){
        factors.put(factor, value);
    }



    public CellState getCellState() {
        return cellState;
    }


    public void changeState(CellState newState){
       cellState = newState;
       validateParameters();
    }

    public void validateParameters() {
        int fertilityThreshold = Integer.parseInt(Configuration.property(Constants.CELL, "fertility.threshold"));

        if(cellState == CellState.WATER || this.getFactor(Factors.HEIGHT) < 0.0)
            this.setFactors(Factors.HEIGHT, 0);

        //fertility of steppe cells might increase in time if there's water
        //once certain level is reached steppe cell would turn to agricultural cell
        if((cellState != CellState.AGRICULTURAL && cellState != CellState.STEPPE) || this.getFactor(Factors.SOIL) < 0.0)
            this.setFactors(Factors.SOIL, 0);
        if(cellState == CellState.AGRICULTURAL && this.getFactor(Factors.SOIL) < fertilityThreshold)
            cellState = CellState.STEPPE;
        if(cellState == CellState.STEPPE && this.getFactor(Factors.SOIL) > fertilityThreshold)
            cellState = CellState.AGRICULTURAL;

        //TODO: add other validations if needed
    }

    public double getOverallCellValue() {
        //todo : designate equation to calculate value
        return 0.5 * this.getFactor(Factors.WATER) + 0.4 * this.getFactor(Factors.SOIL) + 0.1 * this.getFactor(Factors.HEIGHT);
    }
}
