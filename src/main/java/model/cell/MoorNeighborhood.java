package model.cell;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by majk on 08.12.16.
 */
public class MoorNeighborhood extends Neighborhood {

    public MoorNeighborhood(int w, int h) {
        super(w, h);
    }

    @Override
    public Set<CellCoordinates> getNeighbors(CellCoordinates coords) {

        Set<CellCoordinates> neighbors = new HashSet<>();

        int x =  coords.x;
        int y =  coords.y;

        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (!(i == x && j == y)) {
                    CellCoordinates c = getCoords(i, j);
                    if (c != null) neighbors.add(c);
                }
            }
        }
        return neighbors;
    }
}