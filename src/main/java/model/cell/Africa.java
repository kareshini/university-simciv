package model.cell;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import model.cell.Cell;
import model.cell.CellCoordinates;
import model.cell.CellState;
import model.mapsanalysis.FactorReader;
import model.mapsanalysis.OutOfContinentException;
import model.util.Factors;

import java.io.IOException;
import java.util.*;

/**
 * Created by Dominik on 2017-01-06.
 */
public class Africa {
    private final int sizeX;
    private final int sizeY;
    private Map<Factors,Integer> defaultValues;
    public BiMap<CellCoordinates, Cell> continentMap = HashBiMap.create();

    public Africa(int sizeX, int sizeY, Map<Factors, Integer> defaultValues) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.defaultValues = defaultValues;
    }

    public void initContinentMap() throws IOException {
        Map<Factors, FactorReader> factorReaders = getFactorReadersMap(sizeX, sizeY);

        //todo function which compute state based on factories
        CellState state = null;

        for(int i=0;i<sizeX;i++){
            for(int j=0;j<sizeY;j++){
                CellCoordinates cellCoordinates = new CellCoordinates(i,j);

                Map<Factors, Integer> factorValues = getFactorValuesMap(factorReaders, defaultValues, cellCoordinates);

                Cell newCell = new Cell(state, i*sizeY+j, factorValues);
                continentMap.put(cellCoordinates,newCell);
            }
        }
    }

    private Map<Factors, Integer> getFactorValuesMap(Map<Factors, FactorReader> factorReaders, Map<Factors, Integer> defaultValues, CellCoordinates cellCoordinates) {
        Map<Factors,Integer> factorValues = new HashMap<>();
        Iterator it = Arrays.stream(Factors.values()).iterator();
        while(it.hasNext()){
            Factors factor = (Factors) it.next();
            if(factor == Factors.WOOD) {
                continue;
            }
            try {
                factorValues.put(factor,factorReaders.get(factor).getFactoryValue(cellCoordinates));
            } catch (OutOfContinentException e) {
                factorValues.put(factor,defaultValues.get(factor));
            }
        }
        return factorValues;
    }

    private Map<Factors, FactorReader> getFactorReadersMap(int sizeX, int sizeY) throws IOException {
        Map<Factors,FactorReader> factorReaders = new HashMap<>();
        Iterator it = Arrays.stream(Factors.values()).iterator();

        while(it.hasNext()){
            Factors factor = (Factors) it.next();
            if(factor == Factors.WOOD) {
                continue;
            }
            factorReaders.put(factor, FactorReader.createInstance(factor, sizeX, sizeY));
        }
        return factorReaders;
    }
}
