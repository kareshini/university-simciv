package model.cell;

/**
 * Created by michal on 19.11.16.
 */
public enum CellState {
    WATER, STEPPE, DESERT, AGRICULTURAL, FOREST, MOUNTAIN
}
