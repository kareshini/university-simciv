package model.cell.modifiers;

import com.google.common.collect.BiMap;
import model.agent.Agent;
import model.cell.Cell;
import model.cell.CellCoordinates;

import java.util.Set;

/**
 * Created by michal on 10.01.17.
 */
public abstract class CellModifier {
    protected Cell cell;

    public CellModifier(Cell cell){
        this.cell = cell;
    }
    public abstract void perform(Agent agent, Set<CellCoordinates> neighbors, BiMap<CellCoordinates, Cell> cellMap);

}
