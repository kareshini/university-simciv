package model.cell.modifiers;

import com.google.common.collect.BiMap;
import model.agent.Agent;
import model.cell.Cell;
import model.cell.CellCoordinates;
import model.util.Configuration;
import model.util.Constants;
import model.util.Factors;

import java.util.Set;

/**
 * Created by michal on 10.01.17.
 */
public class FertilityUpdate extends CellModifier {
    public FertilityUpdate(Cell cell){
        super(cell);
    }

    @Override
    public void perform(Agent agent, Set<CellCoordinates> neighbors, BiMap<CellCoordinates, Cell> cellMap) {
        int popWeight = Integer.parseInt(Configuration.property(Constants.CELL, "population.weight"));
        int waterWeight = Integer.parseInt(Configuration.property(Constants.CELL, "water.weight"));
        cell.setFactors(Factors.SOIL, cell.getFactor(Factors.SOIL) + (agent.getPopulation() * popWeight) + (cell.getFactor(Factors.WATER) * waterWeight));
        cell.validateParameters();

        //todo validate formula





    }
}
