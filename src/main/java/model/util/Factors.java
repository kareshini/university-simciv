package model.util;

/**
 * Created by Dominik on 2016-11-20.
 */
public enum Factors {
    WATER,WOOD,SOIL,CLIMATE,TEMPERATURE,HEIGHT
}
