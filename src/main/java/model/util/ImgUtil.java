package model.util;

import model.mapsanalysis.factories.FactorsMapFileFactory;
import model.mapsanalysis.strategies.LuminanceValue;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Dominik on 2016-11-20.
 */
public class ImgUtil {
    public static void drawNet(BufferedImage image, int x, int y){
        drawNet(image,Color.BLACK,x,y);
    }
    public static void drawNet(BufferedImage image, Color lineColor, int x, int y){
        int width = image.getWidth(), height = image.getHeight();

        Graphics g = image.getGraphics();
        double offsetX = ((double)width) /  x;
        double offsetY = ((double)height) / y;

        g.setColor(lineColor);

        for (int i = 1; i < x; i++) {
            g.drawLine((int) (i * offsetX), 0, (int) (i * offsetX), height);
        }
        for (int i = 1; i < y; i++) {
            g.drawLine(0, (int) (i * offsetY), width, (int) (i * offsetY));
        }
    }
    public static BufferedImage readImage(File path) throws IOException {
        return ImageIO.read(path);
    }
    public static void saveJPG(BufferedImage image, File savePathJPG) throws IOException {
        savePathJPG.createNewFile();
        ImageIO.write(image, "jpg", savePathJPG);
    }

    public BufferedImage scaleImage(BufferedImage before, int width, int height) {
        BufferedImage after = new BufferedImage(width, height, before.getType());
        Graphics2D g = after.createGraphics();
        g.drawImage(before, 0, 0, width, height, null);
        g.dispose();

        return after;
    }

    public static BufferedImage toGray(BufferedImage original) {
        return toGray(original,0.21f,0.71f,0.07f);
    }
    public static BufferedImage toGray(BufferedImage original,float redIntensity, float greenIntensity,float blueIntensity) {
        int alpha, red, green, blue;
        int newPixel;

        BufferedImage lum = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());

        for (int i = 0; i < original.getWidth(); i++) {
            for (int j = 0; j < original.getHeight(); j++) {
                // Get pixels by R, G, B
                red = new Color(original.getRGB(i, j)).getRed();
                green = new Color(original.getRGB(i, j)).getGreen();
                blue = new Color(original.getRGB(i, j)).getBlue();

                red = (int) (redIntensity * red + greenIntensity * green + blueIntensity * blue);
                // Return back to original format
                newPixel = rgbToInt( red, red, red);

                // Write pixels into image
                lum.setRGB(i, j, newPixel);

            }
        }
        return lum;
    }

    public static int rgbToInt(int red, int green, int blue) {
        return new Color(red,green,blue).getRGB();
    }
    public static Color getTheBrightestColor(BufferedImage image){
        float brightest=0.0f,luminance;
        int brightestX = 0,brightestY = 0;
        LuminanceValue lum = new LuminanceValue();


        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                luminance =  lum.getPixelValue(image,i,j);
                if(luminance>brightest) {
                    brightest=luminance;
                    brightestX = i;
                    brightestY = j;
                }
            }
        }
        return new Color(image.getRGB(brightestX,brightestY));
    }

    public static void main(String[] args) throws IOException {
        //,0.1529f,0.5016f,0.7414f  -- water.jpg
      //  saveJPG(toGray(ImageIO.read(FactorsMapFileFactory.getFile(Factors.TEMPERATURE))), new File("src/test/resources/model/mapsanalysis/tempGray.jpg"));
        BufferedImage bi = ImageIO.read(FactorsMapFileFactory.getFile(Factors.HEIGHT));
        ImgUtil.drawNet(bi,30,30);
        saveJPG(bi,new File("src/test/resources/model/mapsanalysis/height30.jpg"));
    }
}
