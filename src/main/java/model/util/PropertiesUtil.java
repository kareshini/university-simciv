package model.util;

import java.io.*;
import java.util.*;

/**
 * Created by Dominik on 2016-11-22.
 */
public class PropertiesUtil {
    public static Properties getProperties(PropertiesFiles e) {
        String path = propPath(e);
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream(path);
            prop.load(input);
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return prop;
    }
   /* public static void saveProperties(PropertiesFiles e, Map<String,String> data) throws IOException {
        String path = propPath(e);
        Properties prop = new Properties();
        OutputStream output = null;

        try {
            output = new FileOutputStream(path);
            data.forEach((k,v) -> prop.setProperty(k,v));
            prop.store(output,null);
        }finally {
            if(output!=null){
                try {
                    output.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }*/ // this method overrides configs.

    private static String propPath(PropertiesFiles pf) {
        switch(pf){
            case MAP_ANALYSIS:
                return "src/main/resources/config/mapsAnalysis.properties";
            case CELL_CONSTANTS:
                return "src/main/resources/config/cellConstants.properties";
            case AUTOMATON_CONFIG:
                return "src/main/resources/config/automatonConfig.properties";
            default:
                return "src/main/resources/config/automatonConfig.properties";
        }
    }
}
