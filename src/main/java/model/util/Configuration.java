package model.util;

import java.util.Properties;

/**
 * Created by Dominik on 2016-12-11.
 */
public class Configuration {
    private static Configuration instance;
    private final Properties cellConstants;
    private final Properties mapsAnalysis;
    private final Properties automatonConfig;


    private Configuration(Properties cellConstants, Properties mapsAnalysis, Properties automatonConfig) {
        this.cellConstants = cellConstants;
        this.mapsAnalysis = mapsAnalysis;
        this.automatonConfig = automatonConfig;
    }

    public static String property(Constants file, String propertyName) {
        initialize();
        switch(file) {
            case CELL:
                return instance.cellConstants.getProperty(propertyName);
            case MAPS_ANALYSIS:
                return instance.mapsAnalysis.getProperty(propertyName);
            case AUTOMATON:
                return instance.automatonConfig.getProperty(propertyName);
        }
        return null;
    }

    private static void initialize() {
        if(instance == null) {
            Properties propCell = PropertiesUtil.getProperties(PropertiesFiles.CELL_CONSTANTS);
            Properties propAnalysis = PropertiesUtil.getProperties(PropertiesFiles.MAP_ANALYSIS);
            Properties propAutomaton = PropertiesUtil.getProperties(PropertiesFiles.AUTOMATON_CONFIG);
            instance = new Configuration(propCell, propAnalysis, propAutomaton);
        }
    }

}
