package model.util;

/**
 * Created by Dominik on 2016-11-22.
 */
public enum PropertiesFiles {
    MAP_ANALYSIS, CELL_CONSTANTS, AUTOMATON_CONFIG
}
