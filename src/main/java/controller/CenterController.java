package controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import view.ImageViewPane;


public class CenterController {

    private ImageView mainImageView = new ImageView();
    private ImageViewPane imageViewPane = null;
    @FXML
    private BorderPane mainWrapper;

    // Dynamic labels

    @FXML
    private Label fpsLabel;

    private Main mainApp;

    //************************************************* Public methods *************************************************************************

    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;

        fpsLabel.textProperty().bind(mainApp.fpsLabel);

        initializeMainImage();
        mainImageView.imageProperty().bind(mainApp.mainImage);
    }

    private void initializeMainImage() {
        mainImageView.setPreserveRatio(true);
        imageViewPane = new ImageViewPane(mainImageView);
        mainWrapper.setCenter(imageViewPane);
    }

}
