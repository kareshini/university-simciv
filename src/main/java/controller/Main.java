package controller;


import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.automaton.Automaton;
import model.cell.Africa;
import model.mapsanalysis.factories.FactorsMapFileFactory;
import model.util.Factors;
import view.ImageViewPane;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class Main extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    private List<BufferedImage> factorImages;
    private BufferedImage africa = null;
    private int textAreaLineCounter = 0;

    public StringProperty yearLabel, populationLabel, settlementLabel, textArea, fpsLabel;
    public SimpleObjectProperty<Image> mainImage;
    public double fps = 1.0;
    public boolean isPlay = false;
    public Timeline timeline = new Timeline();

    //tests
    private boolean counter = true;
    private BufferedImage tmp = null;

    //model
    private Automaton automaton;


    public Main() {
        try {
            //VIEW
            this.mainImage = new SimpleObjectProperty<>();//(I);
            this.yearLabel = new SimpleStringProperty();
            this.populationLabel = new SimpleStringProperty();
            this.settlementLabel = new SimpleStringProperty();
            this.fpsLabel = new SimpleStringProperty();
            this.textArea = new SimpleStringProperty();
            this.textArea.set("");

            this.factorImages = new ArrayList<>(4);
            factorImages.add(ImageIO.read(FactorsMapFileFactory.getFile(Factors.SOIL)));
            factorImages.add(ImageIO.read(FactorsMapFileFactory.getFile(Factors.HEIGHT)));
            factorImages.add(ImageIO.read(FactorsMapFileFactory.getFile(Factors.TEMPERATURE)));
            factorImages.add(ImageIO.read(FactorsMapFileFactory.getFile(Factors.WATER)));

            timeline.setCycleCount(Timeline.INDEFINITE);
            setAutomaton();
            setSpeed(1);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public List<BufferedImage> getFactorImages() {
        return factorImages;
    }

    public void setSpeed(final double framesPerSecond) {
        if (framesPerSecond < 1)
            this.fps = 1;
        else if (framesPerSecond > 15)
            this.fps = 15;
        else
            this.fps = framesPerSecond;


        double timeInterval = 1000.0 / fps;
        KeyFrame keyFrame = new KeyFrame(
                Duration.millis(timeInterval),
                event -> updateMainImage()
        );

        timeline.stop();
        timeline.getKeyFrames().setAll(keyFrame);
        if (isPlay) {
            timeline.play();
        }

    }

    private void addLineToTextArea(String line) {
        //TODO 50 change to config properties
        String tmp = "";
        if (textAreaLineCounter >= 50) {
            tmp = textArea.get().replaceFirst("^.*\n", "");
        } else {
            tmp = textArea.get();
            textAreaLineCounter++;
        }
        textArea.set(tmp + line);
    }

    private void updateMainImage() {
        System.out.println("..timer.. fps=" + fps);
        yearLabel.set(String.valueOf(fps));
        if (counter) {
            addLineToTextArea("Change map to : main \n");
            mainImage.set(SwingFXUtils.toFXImage(africa, null));
            counter = false;
        } else {
            addLineToTextArea("Change map to : tmp \n");
            mainImage.set(SwingFXUtils.toFXImage(tmp, null));
            counter = true;
        }

    }

    //********************************************************************************************
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("SimCiv");

        try {
            africa = ImageIO.read(new File("src/main/resources/africaMap.jpg"));
            tmp = ImageIO.read(FactorsMapFileFactory.getFile(Factors.TEMPERATURE));
            mainImage.set(SwingFXUtils.toFXImage(africa, null));
        } catch (IOException e) {
            e.printStackTrace();
        }
        yearLabel.set("0");
        populationLabel.set("0");
        settlementLabel.set("0%");
        fpsLabel.set("0");

        initRootLayout();
        initCenter();
        initAside();
    }

    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ImageViewPane.class.getResource("RootLayout.fxml"));
            rootLayout = loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);

            loader.setLocation(RootController.class.getResource("RootController.java"));
            RootController controller = loader.getController();
            controller.setMainApp(this);

            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initCenter() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ImageViewPane.class.getResource("Center.fxml"));
            AnchorPane center = loader.load();

            rootLayout.setCenter(center);

            loader.setLocation(CenterController.class.getResource("CenterController.java"));
            CenterController controller = loader.getController();
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initAside() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ImageViewPane.class.getResource("Aside.fxml"));
            AnchorPane aside = loader.load();

            rootLayout.setRight(aside);

            loader.setLocation(CenterController.class.getResource("AsideController.java"));

            AsideController controller = loader.getController();
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setAutomaton(){
        Map<Factors,Integer> defaultValues = new HashMap<>();
        //todo read values from config
        Arrays.stream(Factors.values()).forEach(factor -> defaultValues.put(factor,30));
        Africa continent= new Africa(100,100,defaultValues);
        //setSpeed uses updateMainImage which is triggered every new frame.
        this.automaton = new Automaton(continent.continentMap,null,null,100,100);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
