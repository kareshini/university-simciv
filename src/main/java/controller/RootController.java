package controller;

import javafx.application.HostServices;
import javafx.fxml.FXML;

import java.io.File;

/**
 * Created by Dominik on 2016-12-06.
 */
public class RootController {

    private Main mainApp;
    private HostServices hostServices;
    private File pdf = new File("src/main/resources/documentation/doc.pdf");

    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
        hostServices = mainApp.getHostServices();
    }

    @FXML
    private void handleDocumentation(){
        hostServices.showDocument(pdf.getAbsolutePath());
    }

}
