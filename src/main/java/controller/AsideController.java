package controller;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import view.ImageViewPane;

/**
 * Created by Dominik on 2016-12-06.
 */
public class AsideController {

    // Buttons

    @FXML
    private Button startButton;
    @FXML
    private Button stopButton;
    @FXML
    private Button fasterButton;
    @FXML
    private Button slowerButton;
    @FXML
    private Button defaultViewButton;

    // Dynamic labels

    @FXML
    private Label yearLabel;
    @FXML
    private Label populationLabel;
    @FXML
    private Label settlementLabel;


    // Info , logs

    @FXML
    private TextArea textArea;

    // Factors

    @FXML
    private TabPane tabPane;

    private Main mainApp;

    //************************************************ Button handlers **************************************************************************

    @FXML
    private void handleStartButton() {
        mainApp.populationLabel.set("start");
        mainApp.timeline.play();
        mainApp.isPlay = true;
    }

    @FXML
    private void handleStopButton() {
        mainApp.populationLabel.set("stop");
        mainApp.timeline.stop();
        mainApp.isPlay = false;
    }

    @FXML
    private void handleFasterButton() {
        mainApp.populationLabel.set("faster");
        mainApp.setSpeed(mainApp.fps + 0.5);
        mainApp.fpsLabel.set(String.valueOf(mainApp.fps));
    }

    @FXML
    private void handleSlowerButton() {
        mainApp.populationLabel.set("slower");
        mainApp.setSpeed(mainApp.fps - 0.5);
        mainApp.fpsLabel.set(String.valueOf(mainApp.fps));
    }

    @FXML
    private void handleDefaultViewButton() {

    }

    //************************************************* Public methods *************************************************************************

    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
        yearLabel.textProperty().bind(mainApp.yearLabel);
        populationLabel.textProperty().bind(mainApp.populationLabel);
        settlementLabel.textProperty().bind(mainApp.settlementLabel);

        textArea.textProperty().bind(mainApp.textArea);
        textArea.setWrapText(true);
        mainApp.textArea.addListener(event -> textArea.setScrollTop(Double.MAX_VALUE));

        initializeTabPane();
        textArea.setEditable(false);
    }

    //************************************************* Initialize *************************************************************************

    private void initializeTabPane() {
        for (int i = 0; i < 4; i++) {
            Tab tab = new Tab();
            tab.setText("Factor" + (i + 1));
            HBox hbox = new HBox();
            ImageView imageView = new ImageView(SwingFXUtils.toFXImage(mainApp.getFactorImages().get(i), null));
            imageView.setPreserveRatio(true);
            hbox.getChildren().add(new ImageViewPane(imageView));
            hbox.setAlignment(Pos.CENTER);
            tab.setContent(hbox);
            tabPane.getTabs().add(tab);
        }
    }
}
