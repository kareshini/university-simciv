package lib;

/**
 * Created by Dominik on 2017-01-07.
 */
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import model.agent.Agent;
import model.cell.CellCoordinates;
import model.civilisation.Marker;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
public class BimapTest {

    @Test
    public void testBiMapForAgentAndCellCoordiantes(){
        CellCoordinates c1 = new CellCoordinates(0,0);
        CellCoordinates c2 = new CellCoordinates(0,1);

        Agent a1 = new Agent(Marker.BLACK,1001);
        Agent a2 = new Agent(Marker.MAROON,1002);

        BiMap<CellCoordinates,Agent> map =  HashBiMap.create();

        map.put(c1,a1);
        map.put(c2,a2);

        assertThat(map.get(c1)).isEqualTo(a1);

        a2.updatePopulation(10);

        assertThat(map.get(c2)).isNotNull();
        assertThat(map.get(c2)).isEqualTo(a2);

        BiMap<Agent,CellCoordinates> inversed = map.inverse();

        assertThat(inversed.get(a1)).isEqualTo(c1);

        assertThat(inversed.get(a2)).isNotNull();
        assertThat(inversed.get(a2)).isEqualTo(c2);

    }
    @Test
    public void getHashObjectFromBimap(){
        Tmp ob1=new Tmp(2);
        Tmp ob2=new Tmp(3);

        Integer i1 = 1;
        Integer i2 = 2;


        BiMap<Integer,Tmp> map =  HashBiMap.create();

        map.put(i1,ob1);
        map.put(i2,ob2);

        assertThat(map.get(i1)).isNotNull();
        assertThat(map.get(i1)).isEqualTo(ob1);

        ob2.val=3;

        assertThat(map.get(i2)).isEqualTo(ob2);

    }
    class Tmp {
        int val;
        public Tmp(int val){
            this.val=val;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Tmp tmp = (Tmp) o;

            return val == tmp.val;

        }

        @Override
        public int hashCode() {
            return val;
        }
    }
}

