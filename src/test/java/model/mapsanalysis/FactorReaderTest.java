package model.mapsanalysis;

/**
 * Created by Dominik on 2017-01-06.
 */
import model.cell.CellCoordinates;
import model.util.Factors;
import org.assertj.core.data.Percentage;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

public class FactorReaderTest {

    @Test
    public void isContinentForRGBTest() throws IOException {
        FactorReader reader = FactorReader.createInstance(Factors.CLIMATE,10,10);
//        System.out.println(reader.isContinent(new CellCoordinates(0,0)));

        Assert.assertTrue("0:9 should not be a Continent ",!reader.isContinent(new CellCoordinates(0,9)));
        Assert.assertTrue("5:5 should be a Continent ",reader.isContinent(new CellCoordinates(5,5)));
        Assert.assertTrue("4:7 should be a Continent ",reader.isContinent(new CellCoordinates(4,7)));
        Assert.assertTrue("0:5 should not be a Continent ",!reader.isContinent(new CellCoordinates(0,5)));
        Assert.assertTrue("0:0 should not be a Continent ",!reader.isContinent(new CellCoordinates(0,0)));
    }
    @Test
    public void isContinentForLuminanceTest() throws IOException {
        //for temperature we have to have irrelevant color because country lines are white
        FactorReader reader = FactorReader.createInstance(Factors.WATER,10,10);

        Assert.assertTrue("0:9 should not be a Continent ",!reader.isContinent(new CellCoordinates(0,9)));
        Assert.assertTrue("5:5 should be a Continent ",reader.isContinent(new CellCoordinates(5,5)));
        Assert.assertTrue("4:7 should be a Continent ",reader.isContinent(new CellCoordinates(4,7)));
        Assert.assertTrue("0:5 should not be a Continent ",!reader.isContinent(new CellCoordinates(0,5)));
        Assert.assertTrue("0:0 should not be a Continent ",!reader.isContinent(new CellCoordinates(0,0)));
    }

    @Test
    public void climatRgbCoordinatesValueTest() throws IOException, OutOfContinentException {
        FactorReader reader = FactorReader.createInstance(Factors.CLIMATE,20,20);
        List<CellCoordinates> coordsList = new ArrayList<>();
        for(int i = 0 ; i<20;i++){
            coordsList.add(new CellCoordinates(11,i));
        }

        CellCoordinates redCoord = coordsList.get(3);

        assertThat(redCoord).isNotNull();
        assertThat(reader.getFactoryValue(redCoord)).isEqualTo(80);

        CellCoordinates orangeAndRed = coordsList.get(6);
        assertThat(orangeAndRed).isNotNull();
        assertThat(reader.getFactoryValue(orangeAndRed)).isBetween(75,80);

        CellCoordinates lightBlue = coordsList.get(8);
        assertThat(lightBlue).isNotNull();
        assertThat(reader.getFactoryValue(lightBlue)).isEqualTo(50);

        CellCoordinates last = coordsList.get(19);
        assertThat(last).isNotNull();
        assertThat(reader.getFactoryValue(last)).isCloseTo(70, Percentage.withPercentage(10));

    }
    @Test
    public void heightHeatCoordinatesValueTest() throws IOException {
        FactorReader reader = FactorReader.createInstance(Factors.HEIGHT,20,20);

        CellCoordinates redCoord = new CellCoordinates(14,11);
        CellCoordinates yeallowCoord = new CellCoordinates(8,2);
        CellCoordinates greenCoord = new CellCoordinates(3,4);
        CellCoordinates blueCoord = new CellCoordinates(0,6);

        Integer redVal = null;
        Integer yeallowVal = null;
        Integer greenVal = null;
        Integer blueVal = null;
        try {
            redVal = reader.getFactoryValue(redCoord);
            yeallowVal = reader.getFactoryValue(yeallowCoord);
            greenVal = reader.getFactoryValue(greenCoord);
            blueVal = reader.getFactoryValue(blueCoord);
        } catch (OutOfContinentException e) {
            e.printStackTrace();
        }

        assertThat(redVal).isNotNull();
        assertThat(yeallowVal).isNotNull();
        assertThat(greenVal).isNotNull();
        assertThat(blueVal).isNotNull();

//        System.out.println(redVal);
//        System.out.println(yeallowVal);
//        System.out.println(greenVal);
//        System.out.println(blueVal);

        assertThat(redVal).isGreaterThan(yeallowVal);
        assertThat(yeallowVal).isGreaterThan(greenVal);
        assertThat(greenVal).isGreaterThan(blueVal);

    }
}
