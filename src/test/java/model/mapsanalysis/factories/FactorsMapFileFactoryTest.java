package model.mapsanalysis.factories;

import org.junit.Assert;
import org.junit.Test;
import model.util.Factors;

import java.io.File;

/**
 * Created by majk on 26.11.16.
 */
public class FactorsMapFileFactoryTest {
    @Test
    public void getFile() throws Exception {
        Factors factor = Factors.CLIMATE;
        File f = FactorsMapFileFactory.getFile(factor);
        Assert.assertTrue(f.getAbsolutePath().contains("climat.jpg"));
    }

    @Test
    public void getFactory() throws Exception {
        File f = new File("src/main/resources/factories/water.jpg");
        Factors factor = FactorsMapFileFactory.getFactory(f);
        Assert.assertEquals(factor, Factors.WATER);
    }

}