package model.mapsanalysis.factories;

import model.mapsanalysis.strategies.HeatValueTest;
import model.mapsanalysis.strategies.LuminanceValue;
import model.mapsanalysis.strategies.PixelValueStrategy;
import model.mapsanalysis.strategies.RgbValue;
import model.util.Factors;
import model.util.PropertiesFiles;
import model.util.PropertiesUtil;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;
/**
 * Created by majk on 26.11.16.
 */
public class StrategiesFactoryTest {

    @Test
    public void getRgbStrategy(){
        PixelValueStrategy strategy = StrategiesFactory.getPixelValueStrategy(
                Factors.CLIMATE,
                PropertiesUtil.getProperties(PropertiesFiles.MAP_ANALYSIS)
        );

        assertThat(strategy instanceof RgbValue);
    }
    @Test
    public void getLuminanceStrategy(){
        PixelValueStrategy strategy = StrategiesFactory.getPixelValueStrategy(
                Factors.TEMPERATURE,
                PropertiesUtil.getProperties(PropertiesFiles.MAP_ANALYSIS)
        );

        assertThat(strategy instanceof LuminanceValue);
    }

    @Test
    public void getCustomLuminanceStrategy(){

    }
    @Test
    public void getHeatStrategy(){
        PixelValueStrategy strategy = StrategiesFactory.getPixelValueStrategy(
                Factors.HEIGHT,
                PropertiesUtil.getProperties(PropertiesFiles.MAP_ANALYSIS)
        );

        assertThat(strategy instanceof HeatValueTest);
    }

}