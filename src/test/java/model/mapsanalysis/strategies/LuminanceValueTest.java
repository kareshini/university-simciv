package model.mapsanalysis.strategies;

import model.mapsanalysis.factories.FactorsMapFileFactory;
import model.mapsanalysis.factories.StrategiesFactory;
import model.util.Factors;
import model.util.PropertiesFiles;
import model.util.PropertiesUtil;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import static org.assertj.core.api.Assertions.*;

/**
 * Created by Dominik on 2017-01-06.
 */
public class LuminanceValueTest {
    private static BufferedImage tempBi;
    private static LuminanceValue tempStrategy;
    private static BufferedImage waterBi;
    private static LuminanceValue waterStrategy;

    @BeforeClass
    public static void init() throws IOException {
        tempBi = ImageIO.read(FactorsMapFileFactory.getFile(Factors.TEMPERATURE));
        tempStrategy = (LuminanceValue) StrategiesFactory.getPixelValueStrategy(
                Factors.TEMPERATURE,
                PropertiesUtil.getProperties(PropertiesFiles.MAP_ANALYSIS)
        );

        waterBi = ImageIO.read(FactorsMapFileFactory.getFile(Factors.WATER));
        waterStrategy = (LuminanceValue) StrategiesFactory.getPixelValueStrategy(
                Factors.WATER,
                PropertiesUtil.getProperties(PropertiesFiles.MAP_ANALYSIS)
        );

    }
    @Test
    public void getPixelValueDiffTestTemp() throws Exception {

        float bright = tempStrategy.getPixelValue(tempBi, 650 ,355);
        float darkness1 = tempStrategy.getPixelValue(tempBi, 280 ,55);
        float darkness2 = tempStrategy.getPixelValue(tempBi, 280, 105);
        float darkness3 = tempStrategy.getPixelValue(tempBi, 240, 130);
        float darkness4 = tempStrategy.getPixelValue(tempBi, 150, 180);
        float darkness5 = tempStrategy.getPixelValue(tempBi, 170, 240);

        assertThat(bright).isGreaterThan(darkness1);
        assertThat(darkness1).isGreaterThan(darkness2);
        assertThat(darkness2).isGreaterThan(darkness3);
        assertThat(darkness3).isGreaterThan(darkness4);
        assertThat(darkness4).isGreaterThan(darkness5);
    }

    @Test
    public void getPixelValueTestTemp(){
        float lum_water = tempStrategy.getPixelValue(tempBi, 0, 0);
        assertThat(lum_water * 255).isBetween(200f,255f);

        float lum_dark = tempStrategy.getPixelValue(tempBi, 95, 240);
        assertThat(lum_dark*256).isBetween(30f,55f);
    }

    @Test
    public void getPixelValueDiffTestWater() throws Exception {

        float darkness1 = waterStrategy.getPixelValue(waterBi, 300, 214);
        float darkness2 = waterStrategy.getPixelValue(waterBi, 406, 224);
        float darkness3 = waterStrategy.getPixelValue(waterBi, 530, 290);
        float darkness4 = waterStrategy.getPixelValue(waterBi, 280, 320);
        float darkness5 = waterStrategy.getPixelValue(waterBi, 474, 420);

        assertThat(darkness1).isGreaterThan(darkness2);
        assertThat(darkness2).isGreaterThan(darkness3);
        assertThat(darkness3).isGreaterThan(darkness4);
        assertThat(darkness4).isGreaterThan(darkness5);
    }
}
