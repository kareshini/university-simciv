package model.mapsanalysis.strategies;

/**
 * Created by Dominik on 2017-01-06.
 */

import model.mapsanalysis.factories.FactorsMapFileFactory;
import model.mapsanalysis.factories.StrategiesFactory;
import model.util.Factors;
import model.util.PropertiesFiles;
import model.util.PropertiesUtil;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class HeatValueTest {
    private static BufferedImage heightBi;
    private static HeatValue heightStrategy;
    private static BufferedImage soilBi;
    private static HeatValue soilStrategy;

    @BeforeClass
    public static void init() throws IOException {
        heightBi = ImageIO.read(FactorsMapFileFactory.getFile(Factors.HEIGHT));
        heightStrategy = (HeatValue) StrategiesFactory.getPixelValueStrategy(
                Factors.HEIGHT,
                PropertiesUtil.getProperties(PropertiesFiles.MAP_ANALYSIS)
        );

        soilBi = ImageIO.read(FactorsMapFileFactory.getFile(Factors.SOIL));
        soilStrategy = (HeatValue) StrategiesFactory.getPixelValueStrategy(
                Factors.SOIL,
                PropertiesUtil.getProperties(PropertiesFiles.MAP_ANALYSIS)
        );
    }

    @Test
    public void hsbValuesTest() {
        Map<Color, float[]> hsb = new HashMap<>(4);

        hsb.put(Color.RED, new float[3]);
        hsb.put(Color.YELLOW, new float[3]);
        hsb.put(Color.GREEN, new float[3]);
        hsb.put(Color.BLUE, new float[3]);

        Color.RGBtoHSB(255, 0, 0, hsb.get(Color.RED));
        Color.RGBtoHSB(255, 255, 0, hsb.get(Color.YELLOW));
        Color.RGBtoHSB(0, 255, 0, hsb.get(Color.GREEN));
        Color.RGBtoHSB(0, 0, 255, hsb.get(Color.BLUE));

        assertThat(hsb.get(Color.RED)[0] < hsb.get(Color.YELLOW)[0]);
        assertThat(hsb.get(Color.YELLOW)[0] < hsb.get(Color.GREEN)[0]);
        assertThat(hsb.get(Color.GREEN)[0] < hsb.get(Color.BLUE)[0]);

    }

    @Test
    public void getPixelValueDiffTestHeight() throws Exception {

        float green = heightStrategy.getPixelValue(heightBi, 77, 98);
        float yellow = heightStrategy.getPixelValue(heightBi, 226, 99);
        float red = heightStrategy.getPixelValue(heightBi, 173, 263);
        float blue = heightStrategy.getPixelValue(heightBi, 10, 114);

        assertThat(blue).isLessThan(green);
        assertThat(green).isLessThan(yellow);
        assertThat(yellow).isLessThan(red);
        assertThat(red).isGreaterThan(green);

    }

    @Test
    public void getPixelValueDiffTestSoil() throws Exception {

        float green = soilStrategy.getPixelValue(soilBi, 283, 249);
        float yellow = soilStrategy.getPixelValue(soilBi, 351, 287);
        float red = soilStrategy.getPixelValue(soilBi, 216, 309);
        float blue = soilStrategy.getPixelValue(soilBi, 268, 187);

        float white = heightStrategy.getPixelValue(heightBi, 289, 91);

        assertThat(white).isLessThan(blue);
        assertThat(blue).isLessThan(green);
        assertThat(green).isLessThan(yellow);
        assertThat(yellow).isLessThan(red);
        assertThat(red).isGreaterThan(green);

    }
}
