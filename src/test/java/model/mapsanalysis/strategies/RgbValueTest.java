package model.mapsanalysis.strategies;

import model.mapsanalysis.factories.FactorsMapFileFactory;
import model.mapsanalysis.factories.StrategiesFactory;
import model.util.Factors;
import model.util.PropertiesFiles;
import model.util.PropertiesUtil;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Dominik on 2017-01-06.
 */
public class RgbValueTest {
    private static BufferedImage climatBi;
    private static RgbValue climatStrategy;
    private static BufferedImage woodBi;
    private static RgbValue woodStrategy;


    @BeforeClass
    public static void init() throws IOException {
        climatBi = ImageIO.read(FactorsMapFileFactory.getFile(Factors.CLIMATE));
        climatStrategy = (RgbValue) StrategiesFactory.getPixelValueStrategy(
                Factors.CLIMATE,
                PropertiesUtil.getProperties(PropertiesFiles.MAP_ANALYSIS)
        );
        woodBi = ImageIO.read(FactorsMapFileFactory.getFile(Factors.WOOD));
        woodStrategy = (RgbValue) StrategiesFactory.getPixelValueStrategy(
                Factors.WOOD,
                PropertiesUtil.getProperties(PropertiesFiles.MAP_ANALYSIS)
        );
    }

    @Test
    public void rgbValuesAreLoadedProperlyTestClimat(){
        Map<Integer,Float> map = climatStrategy.getColorToValueMap();

        assertThat(map).containsKeys(-256,-131072,-91904,-12932356,-15961092,-16777012,-7012457,-8196316,-10500250,-26471,-140702).hasSize(11);
    }
    @Test
    public void getPixelValueTestClimat() {
        //red
        float val1 = climatStrategy.getPixelValue(climatBi,114,62);
        assertThat(val1).isGreaterThan(0f);
        assertThat(val1).isEqualTo(0.8f);

        //darkgreen
        float val2 = climatStrategy.getPixelValue(climatBi,186,288);
        assertThat(val2).isGreaterThan(0f);
        assertThat(val2).isEqualTo(0.65f);

        //irrelevant
        float val3 = climatStrategy.getPixelValue(climatBi,50,300);
        assertThat(val3).isLessThan(0f);
    }
    @Test
    public void rgbValuesAreLoadedProperlyTestWOOD(){
        Map<Integer,Float> map = woodStrategy.getColorToValueMap();

        assertThat(map).hasSize(9);
    }
    @Test
    public void getPixelValueTestWood() {
        //light-yellow
        float val1 = woodStrategy.getPixelValue(woodBi,1000,500);
        assertThat(val1).isZero();

        //orange
        float val2 = woodStrategy.getPixelValue(woodBi,1500,1400);
        assertThat(val2).isEqualTo(0.5f);
    }

}
