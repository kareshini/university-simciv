package model.mapsanalysis;

/**
 * Created by Dominik on 2017-01-07.
 */
import model.cell.CellCoordinates;
import model.mapsanalysis.factories.FactorsMapFileFactory;
import model.util.Factors;
import model.util.PropertiesFiles;
import model.util.PropertiesUtil;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static org.assertj.core.api.Assertions.*;
public class MapsTest {
    @Test
    public void methodSetTest(){
        Properties mapsAnalysis = PropertiesUtil.getProperties(PropertiesFiles.MAP_ANALYSIS);
        Arrays.stream(Factors.values()).forEach(factor ->
                assertThat(mapsAnalysis.getProperty(factor.name().toLowerCase() + ".method")).isNotNull()
        );
    }
    @Test
    public void irrelevantsTest(){
        Map<Factors,BufferedImage> imageMap = new HashMap<>();
        Map<Factors,FactorReader> readerMap = new HashMap<>();

        Arrays.stream(Factors.values()).forEach(factor -> {
                    try {
                        imageMap.put(factor, ImageIO.read(FactorsMapFileFactory.getFile(factor)));
                        readerMap.put(factor,FactorReader.createInstance(factor,20,20));
                    } catch (IOException e) {
                        e.printStackTrace();
                        assert false;
                    }
                }
        );

        CellCoordinates zero = new CellCoordinates(0,0);
        Arrays.stream(Factors.values()).forEach(factor -> assertThat(readerMap.get(factor).isContinent(zero)).isFalse());
    }

}
