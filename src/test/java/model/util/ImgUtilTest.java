package model.util;

import model.mapsanalysis.factories.FactorsMapFileFactory;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

import static org.assertj.core.api.Assertions.*;

/**
 * Created by Dominik on 2017-01-06.
 */
public class ImgUtilTest {

    @Test
    public void rgbToIntTest() {
        int color = ImgUtil.rgbToInt(254, 0, 0);
        Color col = new Color(254, 0, 0);
        assertThat(col.getRGB()).isEqualTo(color);
    }

    @Test
    public void getTheBrightestColor() throws IOException {
        BufferedImage bi = ImageIO.read(FactorsMapFileFactory.getFile(Factors.WATER));
        assertThat(ImgUtil.getTheBrightestColor(bi)).isEqualTo(new Color(255, 255, 255));
    }
}
