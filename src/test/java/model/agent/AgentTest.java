package model.agent;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import model.agent.traits.MilitaryTraits;
import model.cell.Cell;
import model.cell.CellCoordinates;
import model.cell.CellState;
import model.cell.MoorNeighborhood;
import model.civilisation.Marker;
import model.agent.traits.EconomicTraits;
import model.util.Factors;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by majk on 19.11.16.
 */
public class AgentTest {

    Agent agent;
    int x,y;
    BiMap<CellCoordinates, Cell> map;
    BiMap<CellCoordinates, Agent> agentMap;

    @BeforeClass
    public static void setup() {
        MigrationHelper.initialize(new MoorNeighborhood(10,10));
    }

    @Before
    public void initialize() {
        map = HashBiMap.create();
        agentMap = HashBiMap.create();
        Marker m = Marker.BLACK;
        CellState s = CellState.AGRICULTURAL;
        x = 5;
        y = 7;

        for(int i = 0; i < 10; i++) {
            for(int j = 0; j < 10; j++) {
                CellCoordinates coords = new CellCoordinates(i,j);
                Agent a = new Agent(m, i*10+j);
                a.updatePopulation(i*1000 + j*100);
                a.traits.military.changeValue(MilitaryTraits.SOLDIERS, j/10 + 0.1);
                a.traits.military.changeValue(MilitaryTraits.WEAPONRY, i/10 + 0.1);
                agentMap.put(coords, a);

                Map<Factors,Integer> factors = new HashMap<>();
                factors.put(Factors.CLIMATE, 10);
                factors.put(Factors.SOIL, 10);
                factors.put(Factors.HEIGHT, 10);
                factors.put(Factors.TEMPERATURE, 10);
                factors.put(Factors.WATER, 10);
                factors.put(Factors.WOOD, 10);


                map.put(coords, new Cell(s, i*10+j,factors));
                m = Marker.values()[(m.ordinal()+1) % Marker.values().length];
                s = CellState.values()[(m.ordinal()+1) % CellState.values().length];
            }
        }
        agent = agentMap.get(new CellCoordinates(x,y));
    }

    @Test
    public void testAccessToTraits() {
        agent.traits.economic.updateValue(EconomicTraits.IMPORT, 10.0);
        Assert.assertEquals(10.0, agent.traits.economic.getValue(EconomicTraits.IMPORT), 1);
    }

    @Test
    public void testPopulationChanges() {
        agent.updatePopulation(-60000);
        Assert.assertEquals(0, agent.getPopulation());

        agent.updatePopulation(70000);
        Assert.assertEquals(agent.maxPop, agent.getPopulation());

        agent.updatePopulation(-5000);
        Assert.assertEquals(agent.maxPop - 5000, agent.getPopulation());
    }

    @Test
    public void testSaturationChanges() {
        agent.updatePopulation(-5000);
        Assert.assertEquals(0.1, agent.getSaturation(), 0.1);

        agent.updatePopulation(15000);
        Assert.assertEquals(0.3, agent.getSaturation(), 0.1);

        agent.updatePopulation(100000);
        Assert.assertEquals(1.0, agent.getSaturation(), 0.1);
    }

    @Test
    public void testCalculateBestAction() {
        agent.updatePopulation(45001);
        agent.setAction(map, agentMap);
        Assert.assertEquals(AgentActions.MIGRATION, agent.getAction());

        agent.updatePopulation(-30000);
        agent.traits.economic.changeValue(EconomicTraits.SERVICES, 8.0);
        agent.traits.economic.changeValue(EconomicTraits.IMPORT, 1.0);
        agent.setAction(map, agentMap);
        Assert.assertEquals(AgentActions.ECONOMIC_DEVELOPMENT, agent.getAction());

        agent.updatePopulation(-30000);
        agent.traits.military.changeValue(MilitaryTraits.TECHNOLOGY, 8.0);
        agent.traits.military.changeValue(MilitaryTraits.SOLDIERS, 4.0);
        agent.setAction(map, agentMap);
        Assert.assertEquals(AgentActions.ATTACK, agent.getAction());
    }

    @Test
    public void testActionPerform() {
        agent.updatePopulation(30000);
        agent.traits.military.changeValue(MilitaryTraits.WEAPONRY, 8.0);
        agent.traits.military.changeValue(MilitaryTraits.SOLDIERS, 4.0);
        agent.setAction(map, agentMap);
        int pop = agent.getPopulation();
        double trait = agent.traits.military.getValue(MilitaryTraits.SOLDIERS);

        agent.performAction(map, agentMap);
        double result = agent.traits.military.getValue(MilitaryTraits.SOLDIERS);

        Assert.assertTrue(pop != agent.getPopulation() && trait != result);
    }

    @Test
    public void testMigration()  {
        agent.updatePopulation(40000);
        agent.traits.military.changeValue(MilitaryTraits.SOLDIERS, 4.0);
        int pop = agent.getPopulation();
        double trait = agent.traits.military.getValue(MilitaryTraits.SOLDIERS);
        agent.setAction(map, agentMap);
        agent.performAction(map, agentMap);
        double result = agent.traits.military.getValue(MilitaryTraits.SOLDIERS);

        Assert.assertTrue(pop != agent.getPopulation() && trait != result);
    }
}