package model.agent;

import model.agent.traits.EconomicTraits;
import model.agent.traits.MilitaryTraits;
import model.agent.traits.TraitHelper;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by majk on 19.11.16.
 */
public class TraitHelperTest {

    TraitHelper t = null;

    @Test
    public void testConstructorWithList() {
        List<Double> l1 = new ArrayList<>();
        l1.add(0.1);
        l1.add(0.2);
        l1.add(0.3);

        t = new TraitHelper(EconomicTraits.class, l1);

        Assert.assertEquals(0.2, t.getValue(EconomicTraits.EXPORT), 0.1);
    }

    @Test
    public void testConstructorWithClassOnly() {
        t = new TraitHelper(MilitaryTraits.class);
        Assert.assertNotNull(t);
    }

    @Test
    public void testUpdatingValue() {
        List<Double> l1 = new ArrayList<>();
        l1.add(0.1);
        l1.add(0.2);
        l1.add(0.3);

        t = new TraitHelper(MilitaryTraits.class, l1);

        t.updateValue(MilitaryTraits.WEAPONRY, 15.6);

        Assert.assertEquals(15.6, t.getValue(MilitaryTraits.WEAPONRY), 0.1);
    }
}