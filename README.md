# Model rozwoju cywilizacji

_Dominik Katszer, Michał Strugała, Michał Ziegler_

---

## Wprowadzenie
#### Opis problemu
Założeniem projektu jest podjęcie próby stworzenia modelu rozwoju terytorialnego cywilizacji w wybranym rejonie świata z uwzględnieniem wybranych czynników geograficznych, ekonomicznych, geopolitycznych. Projekt jest próbą odpowiedzi na pytanie w jakim stopniu proste reguły oparte na automatach komórkowych mogą odwzorowywać procesy historyczne.

Symulacja mająca na celu odtworzyć rozwój cywilizacji jest zaiste interdyscypilnarnym  przedsięwzięciem. Historia jaką znamy pełna jest opowieści o ludzkich dążeniach, wojnach, zdradach, intrygach i innych zjawiskach będących nieodłącznymi elementami ludzkiej natury, które w odpowiednich warunkacj mogą wywrzeć wpływ na całe społeczeństwa. Czy system, u którego podstawy leżą isoty tak nieobliczalne jak ludzie może być modelowany przy pomocy prostych algorytmów? To pytanie na które próbujemy odpowiedzieć w tym projekcie. Choć nie jesteśmy w stanie z dużą dokładnością jak zachowają się ludzie, jesteśmy w stanie przewidzieć pewne tendencje które występują w dużych zgromadzeniach ludzi, na znacznej przestrzeni czasu. Nie jest to jednak łatwe zadanie. Wpływ na zachowanie ludzi ma bardzo wiele drobnych czynników. Czynniki te są jednak ze sobą powiązane, często w trudny do dostrzeżenia sposób. Nawet jeżeli jesteśmy zainteresowani tylko niektórymi z nich, nadmierne uproszczenie modelu może sktukować tym, że przestanie on nawet w przybliżeniu odwzorowywać prawdziwy przebieg wydarzeń. Z każdym czynnikiem który zaś uwzględnimy, wzrasta prawdopodobieństwo błędu, wynikające z niedoskonałości używanych przez nas formuł, oraz z ograniczonych danych jakie posiadamy na temat danej epoki. Nasz model podejmie wyzwanie próby odwzorowania rozwoju cywilizacji %INSERT CIVILISATION NAME HERE% na okresie %INSERT PERIOD HERE% w sposób zbliżony do rzeczywistego przebiegu tego rozwoju.


## Przegląd literatury

#### War, space, and the evolution of Old World complex societies 
<http://www.pnas.org/content/110/41/16384.full>
###### Opis
Celem modelu jest zrozumienie pod jakimi warunkami normy ultraspołeczne i instytucje będą się rozwijać.
//todo
###### Model
* Ogólnie
  * dwu-wymiarowa siatka komórek, każda reprezentująca 100km x 100km   powierzchni
  * komórki podzielone na trzy klasy:
    * step
    * pustynia
    * rolnicze
  * tylko komórki rolnicze są zamieszkiwane przez społeczeństwa
  * komórki mają do czterech sąsiadów (komórki sądiadujące po skosie nie   są uznawana za sąsiadów)
  * każde państwo zaczyna z jedną komórką
  * państwa mogą podbijać inne komórki
  * państwa mogą się rozpadać
* czynniki charakteryzujące komórki
  * rodzaj (step, pustynia, rolnicze)
  * wzniesienie
  * "ultrasociality traits" (obecne lub nie)
  * technologia militarna (obecna lub nie)
* Parametry modelu
  * krok = 2 lata
  * początkowa ilość "ultrasociality traits" = 10
  * początkowa ilość technologii militarnej = 5
  * prawdopodobieństwo dyfuzjii technologii militarnej do sąsiednich komórek  = 0.25 na krok
  * pozostałe:

![Imgur](http://i.imgur.com/Fw5LG8g.png)

* Wojny
  * wojny mają miejsce na sąsiadujących komórkach różnych państw
  * prawdopodobieństwo sukcesu ataku
  * ![image](http://www.pnas.org/content/110/41/16384/embed/graphic-7.gif)
    * ![image](http://www.pnas.org/content/110/41/16384/embed/graphic-6.gif)
    * ![image](http://www.pnas.org/content/110/41/16384/embed/graphic-5.gif)
      * ![image](http://www.pnas.org/content/110/41/16384/embed/graphic-4.gif)
      * u<sub>ij</sub> - "ultrasociality trait" w i'tej komórce j'tego państwa (0 lub 1)
      * S<sub>att</sub> - rozmiar państwa (liczony w komórkach)

#### MayaSim: An Agent-Based Model of the Ancient Maya Social-Ecological System

<http://jasss.soc.surrey.ac.uk/16/4/11.html>

###### Opis
Model MayaSim symuluje system socjo-ekologiczny starożytnych Majów przy użyciu bazującego na agentach automatu komórkowego. Modelowane są takie aspekty jak: wzrost populacji, produkcję rolniczą, degradacje gruntu, klimat, zalesienie oraz sieci handlowe. Agenci reprezentują osady które rozprzestrzeniają się po przestrzeni, która zmienia się pod wpływem klimatu. Pytania na które model próbuje odpowiedzieć to:

* Co powoduje powstawanie gęsto zaludnionych osad starożytnych Majów?
* Czy można przy pomocy symulacji otworzyć trzy okresy w rozwoju cywilizacji Majów?
* Jak symulowany system socjo-ekologiczny rozwija się w zależności od zmiennych warunków?

Model jest umiejscowiony w Ameryce Środkowej. Jest skonstruowany przy pomocy programu Netlogo.
![image](http://jasss.soc.surrey.ac.uk/16/4/11/Figure1.png)
###### Model
* Ogólnie
  * agenci to osady ulokowane na powierzchni mapy
  * rozdzielczość 20 km<sup>2</sup>
  * krok 2 lata
* Czynniki charakteryzujące komórki
  * opady
  * temperatura
  * wzniesienie
  * wody powierzchniowe
    * każda komórka generuje ruchomego "raindrop" agenta, który reprezentuje wodę powierzchniową, ta przemieszcza się na sąsiednie komórki bazując na danych o wzniesieniu
  * Parametry modelu
    * podstawowa produktywność, liczona na podstawie temperatury i obecności wody
      * ![image](http://jasss.soc.surrey.ac.uk/16/4/11/Equation2.png)
    * postęp zalesienia, liczony na podstawie podstawowej produktywności, symulowany jako automat komórkowy
    * produktywność rolnicza
      * ![image](http://jasss.soc.surrey.ac.uk/16/4/11/Equation3.png)
    * "Ecosystem services"
      * ![image](http://jasss.soc.surrey.ac.uk/16/4/11/Equation4.png)
  * Agenci
    * każdy model.agent - osada i utrzymuje co najmniej jedną komórkę j, aby generować plon
    * ![image](http://jasss.soc.surrey.ac.uk/16/4/11/Equation5.png)
      * BCA<sub>j,t</sub> - pełny zysk z rolnictwa
      * κ<sub>j</sub>, α, φ - parametry plonu i pochylenia
      * AG<sub>j,t</sub> - produktywność rolnicza komórki
      * γ - koszty rolnictwa
      * O<sub>j</sub> - koszt podróży związanej z rolnictwem
      * P<sub>j,t</sub> - populacja osady
    * Rrzeczywisty przychód per capita wynika z rolnictwa, "ecosystem serivces", handlu
      * ![image](http://jasss.soc.surrey.ac.uk/16/4/11/Equation6.png)
      * N<sub>i,t</sub> - rozmiar sieci komórek
      * C<sub>i,t</sub> - odległość od środka sieci
      * TC - koszt podróży
    * birth rate = 15%
    * death rate i emigracja spada liniowo z RI
    * osady z populacją poniżej pewnego poziomu są usuwane
    * osady, które zarejestrują poziom emigracji powyżej pewnego poziomu potrzebnego do utrzymania rolnictwa tworzą "agenta migracyjnego", ten używa funkcji użyteczności, aby wybrać lokacje na nową osadę
      * ![image](http://jasss.soc.surrey.ac.uk/16/4/11/Equation8.png)
      * λ - wagi dla kosztów podróży i "ecosystem services",
      * D<sub>j</sub> - dystans od oryginalnej osady


#### Agent-Based Modeling of the Early Minoan Social Organization Structure
<http://www.intelligence.tuc.gr/~angelos/WorkingPaper.pdf>

###### Opis
//todo

###### Model

* Ogólnie
  * rozdzielczość przestrzenna 20 x 25 km
  * komórki 100 x 100 m
* Czynniki charakteryzujące komórki
  * ilość surowców
  * pochylenie gruntu
  * żyzność gleby
    * zależy od ilości pracy wykonanej na komórce przez agentów
  * jakość produkcji
    * ![Imgur](http://i.imgur.com/jYxBmhs.png)
      * P - rozmiar populacji
      * μ - początkowa ilość surowców
      * μ<sub>max</sub> - maksymalna ilość surowców na komórkę
      * P<sub>max</sub> - maksymalna populacja
      * α<sub>i</sub> - wartość [0,1] charakteryzująca produkcję rolniczą na komórce i
        * zależy od pochylenia, 0 dla pochyenia większego niż 45<sup>o</sup>
* Agenci
  * agenci odpowiadają gospodarstwom domowym
  * agenci starają się "przetrwać", poprzez pozyskiwanie i konsumowanie surowców
  * funkcja wartości agenta kultywującego n komórek
    * ![Imgur](http://i.imgur.com/rXvqRnT.png)
  * model.agent potrzebuje pewnej minimalnej wartości aby być zdolnym do prokreacji
    * ![Imgur](http://i.imgur.com/3A2wiGd.png)
      * j - ilość osobników w gospodarswie domowym
      * res<sub>min</sub> - minimalna ilość surowców na osobnika na rok
  * w przypadku przekroczenia tej wartości, może być ona przechowywana na przyszłość
  * agenci mogą migrować jeżeli dana lokacja nie zaspokaja ich potrzeb
  * jeżeli w wyniku prokrecji zostanie przekroczona maksymalna ilość osobników w gospodarstwie domowym, nowy model.agent jest tworzony poprzez rozdzilenie istnijącego agenata na dwa o losowej ilości osobników
  * jeżeli zostanie przekroczona maksymalna ilość osobników na komórkę nowy model.agent jest lokowany w sąsiedniej komórce
  * deathrate = 0.002
  * ![Imgur](http://i.imgur.com/XUYB8Gz.png)
    * r<sub>birth</sub> = 0.003
    * ![Imgur](http://i.imgur.com/gOhV67p.png)


#### Agent-Based Models of Ancient Egypt
###### Opis
//todo

###### Model
* model ukazuje przekształcanie małych osad w większe wioski
* czas 500 lat
* 5x5 km na brzegu Nilu
* gospodarstwa domowe przemieszczają się w celu poprawienia jakości rzycia
* z migracją związany jest koszt
* w każdym kroku osady wybierają pole do kultywacji

## Proponowany model

W naszej symulacji użyjemy automatu komórkowego do reprezentacji powierzchni afryki w %INSERT PERIOD HERE% wieku. Agenci będą u nas reprezentować populacje na poszczególnych komórkach. Każda z populacji będzie należała do jednej cywilizacji. Do zainincjalizowania danych na temat terenu oraz ludności, użyliśmy następujących map:

Mapa klimatu:
![Imgur](http://i.imgur.com/Zrh3Up6.png)

Mapa populacji:
![Imgur](http://i.imgur.com/jUVVQUd.png)

Grunt:
![Imgur](http://i.imgur.com/jYb9K1l.png)

Dostępność wody:
![Imgur](http://i.imgur.com/ryZFgLk.jpg)

Wszystkie mapy zostały automatycznie zparsowene przy użyciu odpowiedniego algorytmu. %TUTAJ MOŻE KRÓTKI OPIS JAK TO ZROBIŁEŚ, DOMINIK? ALBO NIE, JAK UWAŻASZ ;) %

# ##TUTAJ KOŃCZY SIĘ DOKUMENTACJA NA DRUGIE KONSULTACJE##
//todo cele modelu - zdefiniowanie wejść i wyjść z modelu,
założenia algorytmów,
diagramy cykli działań etc.
stosowane podejście w nawiązaniu do opisu literaturowego.
Diagram przypadków użycia (mogą być inne diagramy dynamiczne UML,
ewentualnie diagramy BPMN).


## Symulacja zjawiska - implemntacja

#### Wybór technologii, uzasadnienie
//todo
#### Opis implementacji
//todo diagramy UML i opis implementacji

## Wyniki symulacji
//todo statystyki, wyniki ilościowe i jakościowe, zastosowanie kalibracji i
walidacji, porównanie wyników z danymi rzeczywistymi

## Wnioski

#### Podsumowanie
//todo Zebranie najważniejszych wniosków, Usytuowanie modelu i symulacji na
tle istniejących rozwiązań, Zebranie najważniejszych wyzwań i trudności
 rozpatrywanego problemu, a także opis elementów, które autorzy uważają za
 największy sukces projektu

#### Future Works: proponowane kierunki rozwoju w przyszłości
 //todo